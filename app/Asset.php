<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Asset extends Model
{
    use Sortable;

    protected $guarded = [];
    public $table = "assets";


    protected $fillable = [
        'id', 'code','employee_id', 'item_id', 'encoded_by', 'approved_by',
        'noted_by', 'received by', 'released by', 'encoded_date',
        'approved_date', 'noted_date', 'received_date', 'released_date',
        'transaction_type', 'level', 'is_deleted'
    ];

}
