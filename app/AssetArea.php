<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetArea extends Model
{
    protected $guarded = [];
    protected $fillable = [];

    public function assets()
    {
        return $this->hasOne(Asset::class);
    }
}
