<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetCategory extends Model
{
    protected $guarded = [];
    protected $fillable = [];

    public function assets()
    {
        return $this->hasOne(Asset::class);
    }
}
