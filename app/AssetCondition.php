<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetCondition extends Model
{
    protected $guarded = [];
    public $table = "asset_conditions";
    protected $fillable = ['id', 'user_id', 'asset_id', 'note', 'condition', 'is_deleted', 'created_at', 'updated_at'];
}
