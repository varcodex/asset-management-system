<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $guarded = [];
    public $table = "departments";
    protected $fillable = ['id', 'code', 'name', 'location_id', 'is_deleted', 'created_at', 'updated_at'];
}
