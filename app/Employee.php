<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $guarded = [];
    public $table = "employees";
    protected $fillable = ['id', 'employee_code', 'firstname', 'middlename', 'lastname', 'is_deleted',
                            'department_id', 'created_at', 'updated_at'];
}
