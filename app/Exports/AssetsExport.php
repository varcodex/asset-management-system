<?php

namespace App\Exports;

use App\Asset;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AssetsExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection


    */

    public function headings(): array
    {
        return [
           "Control No.","Category","Area","Assignee Name", "Asset Assigned", "Reciever",
           "Remarks", "Status", "Created at", "Updated at"
        ];
    }

    public function collection()
    {
        $assets = \DB::table('assets')
        ->join('asset_categories', 'assets.asset_categories_id', '=', 'asset_categories.id')
        ->join('asset_areas', 'assets.asset_areas_id', '=', 'asset_areas.id')
        ->select('assets.control_no', 'asset_categories.name as category', 'asset_areas.name as area', 'assets.assignee_name',
                'assets.asset_assigned', 'assets.recieved_from', 'assets.remarks', 'assets.status',
                'assets.created_at', 'assets.updated_at')
        ->get();
        return $assets;
    }
}
