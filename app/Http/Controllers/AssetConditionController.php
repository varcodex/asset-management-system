<?php

namespace App\Http\Controllers;

use App\AssetCondition;
use Illuminate\Http\Request;

class AssetConditionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AssetCondition  $assetCondition
     * @return \Illuminate\Http\Response
     */
    public function show(AssetCondition $assetCondition)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AssetCondition  $assetCondition
     * @return \Illuminate\Http\Response
     */
    public function edit(AssetCondition $assetCondition)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AssetCondition  $assetCondition
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssetCondition $assetCondition)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AssetCondition  $assetCondition
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssetCondition $assetCondition)
    {
        //
    }
}
