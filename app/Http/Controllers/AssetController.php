<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Exports\AssetsExport;
use App\Asset;
use App\AssetCondition;
use App\AssetCategory;
use App\AssetArea;
use App\ItemList;
use App\ItemDetail;
use App\ItemCategory;
use Maatwebsite\Excel\Facades\Excel;
use Auth;
use DataTables;
use DB;
use PDF;
use Session;
use Storage;

class AssetController extends Controller
{
    public function index()
    {

        $data = DB::table('assets')
                ->join('employees', 'employees.id', '=', 'assets.employee_id')
                ->join('item_lists', 'item_lists.id', '=', 'assets.item_id')
                ->select('assets.id as id', 'assets.code as code', 'employees.employee_code as employee_code', 'item_lists.code as item_code',
                        'assets.level as level', 'assets.transaction_type as transaction_type', 'assets.created_at as created_at', 
                        'assets.updated_at as updated_at')
                ->where('assets.is_deleted', '<>', '1')->orderBy('assets.id', 'ASC')->get();

        if (request()->ajax()){
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="/asset/'.$row->id.'" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm viewAsset">View</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('assets_1.index');
    }

    public function create()
    {
        $employees = DB::table('employees')
                ->join('departments', 'departments.id', '=', 'employees.department_id')
                ->leftjoin('locations', 'locations.id', '=', 'departments.location_id')
                ->select('employees.id as id', 'employees.employee_code as employee_code', 'employees.firstname as firstname',
                        'employees.lastname as lastname', 'employees.middlename as middlename', 'employees.is_deleted as is_deleted',
                        'locations.name as locname', 'departments.name as deptname')
                ->where('employees.is_deleted', '<>', '1')->orderBy('employees.id', 'ASC')->get();
        
        $items = $data = DB::table('item_lists')
                        ->join('item_categories', 'item_categories.id', '=', 'item_lists.category_id')
                        ->select('item_lists.id as id', 'item_lists.code as code', 'item_lists.name as name',
                                'item_categories.name as category')
                        ->where('item_lists.is_deleted', '<>', '1')->orderBy('item_lists.id', 'ASC')->get();

        return view('assets_1.create', compact('employees', 'items'));
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'employee' => 'required',
            'item' => 'required',
            'file_name' => 'file|max:20000|mimes:pdf',
        ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        $condition = json_decode($request->condition, true);

        $status = false;
        while (!$status) 
        {
            if($request->code){
                $asset_code = $request->code;
            }else{
                $asset_code = str_random(10);
            }
            
            $result = Asset::where('is_deleted', '<>', '1')->where('code', '=', $asset_code)->orderBy('id', 'ASC')->get();

            if($result)
            {   
                
                $asset = Asset::updateOrCreate(['id' => $request->id],
                                                    ['code' => $asset_code,
                                                    'item_id' => $request->item,
                                                    'encoded_by' => Auth::id(),
                                                    'employee_id' => $request->employee,
                                                    'received_by' => $request->received_by,
                                                    'released_by' => $request->released_by,
                                                    'encoded_date' => now(),
                                                    'received_date' => $request->date_received,
                                                    'released_date' => $request->date_released,
                                                    'transaction_type' => 'recieved',
                                                    'level' => '1',
                                                    'is_deleted' => '0'
                                                    ]);

                $status = true;

                if ($asset->wasRecentlyCreated) {
                    $this->storeFile($asset_code, $request->file_name);
                }else{
                    if($request->file_name){
                        $image_path = public_path('_assetfiless').'\\'.$asset_code.'.pdf';
                        unlink($image_path);
                        $this->storeFile($asset_code, $request->file_name);
                    }
                }

                for($i = 0; $i < count($condition); $i++){
                    AssetCondition::updateOrCreate(['id' => $condition[$i]['id']],
                                                    ['asset_id' => $asset->id,
                                                    'user_id' => Auth::id(),
                                                    'note' => $condition[$i]['note'],
                                                    'condition' => $condition[$i]['condition'],
                                                    'is_deleted' => '0'
                                                    ]);
        
                }

            }
        }

        return response()->json(['success'=>$request->status]);
    }

    public function show($id)
    {

        $employees = DB::table('employees')
                ->join('departments', 'departments.id', '=', 'employees.department_id')
                ->leftjoin('locations', 'locations.id', '=', 'departments.location_id')
                ->select('employees.id as id', 'employees.employee_code as employee_code', 'employees.firstname as firstname',
                        'employees.lastname as lastname', 'employees.middlename as middlename', 'employees.is_deleted as is_deleted',
                        'locations.name as locname', 'departments.name as deptname')
                ->where('employees.is_deleted', '<>', '1')->orderBy('employees.id', 'ASC')->get();
        
        $items = $data = DB::table('item_lists')
                        ->join('item_categories', 'item_categories.id', '=', 'item_lists.category_id')
                        ->select('item_lists.id as id', 'item_lists.code as code', 'item_lists.name as name',
                                'item_categories.name as category')
                        ->where('item_lists.is_deleted', '<>', '1')->orderBy('item_lists.id', 'ASC')->get();

        $assets = DB::table('assets')
                        ->join('employees', 'employees.id', '=', 'assets.employee_id')
                        ->join('item_lists', 'item_lists.id', '=', 'assets.item_id')
                        ->select('assets.id as id', 'assets.code as code', 'employees.employee_code as employee_code', 'item_lists.code as item_code',
                                'assets.level as level', 'assets.transaction_type as transaction_type', 'assets.created_at as created_at', 
                                'assets.updated_at as updated_at')
                        ->where('assets.is_deleted', '<>', '1')->where('assets.id', '=', $id)->orderBy('assets.id', 'ASC')->get();

        $conditions = AssetCondition::where('is_deleted', '<>', '1')->where('asset_id', '=', $id)->orderBy('id', 'ASC')->get();
     
        return view('assets_1.edit', compact('conditions', 'assets', 'employees', 'items'));
    }

    public function edit(Asset $asset)
    {
     
    }

    public function update(Asset $asset)
    {
        
    }

    public function destroy(Asset $asset)
    {
        $asset->delete();
        return back();
    }

    public function tag(Asset $asset)
    {
        $asset->status = "Tag";
        $asset->save();

        Session::flash('flash_message', 'Asset tag successfully');
	    Session::flash('flash_type', 'alert-success');
        return view('assets.show', compact('asset'));
    }

    public function untag(Asset $asset)
    {
        $asset->status = "Untag";
        $asset->save();

        Session::flash('flash_message', 'Asset untag successfully');
	    Session::flash('flash_type', 'alert-success');
        return view('assets.show', compact('asset'));
    }

    private function validataRequest()
    {
        return tap(request()->validate([
                    'assignee_name' => 'required|min:3',
                    'asset_assigned' => 'required|min:3',
                    'recieved_from' => 'required|min:3',

                ]), function (){

                    if (request()->hasFile('file_name')){
                        request()->validate([
                            'file_name' => 'file|max:20000|mimes:pdf'
                        ]);
                    }

                });
    }

    private function storeFile($asset_code, $uploadedFile)
    {
        $uploadedFile->move(public_path('_assetfiles'), $asset_code.'.pdf');
    }

    public function print($code)
    {
        $assets = DB::table('assets')
                        ->join('employees', 'employees.id', '=', 'assets.employee_id')
                        ->join('item_lists', 'item_lists.id', '=', 'assets.item_id')
                        ->select('assets.id as id', 'assets.code as code', 'employees.employee_code as employee_code', 'item_lists.code as item_code',
                                'employees.firstname as firstname', 'employees.middlename as middlename', 'employees.lastname', 
                                'assets.level as level', 'assets.transaction_type as transaction_type', 'assets.created_at as created_at', 
                                'assets.updated_at as updated_at')
                        ->where('assets.is_deleted', '<>', '1')->where('assets.code', '=', $code)->orderBy('assets.id', 'ASC')->get();
        
        $item_ids = Asset::select('item_id')->where('is_deleted', '<>', '1')->where('code', '=', $code)->orderBy('id', 'ASC')->get();

        foreach($item_ids as $item_id){
            $items = ItemDetail::where('is_deleted', '<>', '1')->where('item_id', '=', $item_id->item_id)->orderBy('id', 'ASC')->get();
            $cat_id = ItemList::select('category_id')->where('is_deleted', '<>', '1')->where('id', '=', $item_id->item_id)->orderBy('id', 'ASC')->get();
        }
        foreach($cat_id as $cat_ids){
            if($cat_ids->category_id == '3'){
                return view('report.vehicle', compact('assets', 'items'));
            }elseif($cat_ids->category_id  == '1'){
                return view('report.ar', compact('assets', 'items'));
            }
        }

        
    }

    public function assetReport()
    {
        $data = DB::table('assets')
                        ->join('employees', 'employees.id', '=', 'assets.employee_id')
                        ->join('item_lists', 'item_lists.id', '=', 'assets.item_id')
                        ->join('departments', 'departments.id', '=', 'employees.department_id')
                        ->join('locations', 'locations.id', '=', 'departments.location_id')
                        ->select('assets.id as id', 'assets.code as asset_code', 'employees.employee_code as employee_code', 'employees.firstname as firstname',
                                'employees.middlename as middlename', 'employees.lastname as lastname', 'locations.name as location', 'departments.name as department',
                                'item_lists.code as item_code', 'item_lists.name as item_name', 'assets.transaction_type as transaction_type',
                                'assets.created_at as created_at', 'assets.updated_at as updated_at')
                        ->where('assets.is_deleted', '<>', '1')->orderBy('assets.id', 'ASC')->get();
       
        if (request()->ajax()){
            return Datatables::of($data)->make(true);
        }

        return view('report.assetreport');
    }

/*
    public function export()
    {
        return Excel::download(new AssetsExport, 'assets.xlsx');
    }

    public function generatePDF()
    {
        $assets = Asset::orderBy('id', 'desc')->get();
        $categories = AssetCategory::all();
        $pdf = PDF::loadView('assets.report', compact('assets', 'categories'));

        return $pdf->download('exported_barcode.pdf');
    }
    */
}
