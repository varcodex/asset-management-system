<?php

namespace App\Http\Controllers;

use App\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
use DataTables;
use Alert;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$data = Department::where('is_deleted', '<>', '1')->orderBy('id', 'ASC')->get();
        $data = DB::table('departments')
                ->join('locations', 'locations.id', '=', 'departments.location_id')
                ->select('departments.id as id', 'departments.code as deptcode', 'departments.name as deptname',
                        'locations.name as locname', 'departments.created_at as created_at', 'departments.updated_at as updated_at')
                ->where('departments.is_deleted', '<>', '1')->orderBy('departments.id', 'ASC')->get();

        $locations = DB::table('locations')
                ->select('id', 'code', 'name')
                ->where('is_deleted', '<>', '1')->orderBy('name', 'ASC')->get();

        if (request()->ajax()){
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editDepartment">Edit</a>';

                    $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteDepartment">Delete</a>';

                        return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('settings.department.index', compact('locations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'department_code' => 'required|min:2',
            'department_name' => 'required|min:3',
            'location' => 'required',
        ]);

            if ($validator->fails())
            {
                return response()->json(['errors'=>$validator->errors()->all()]);
            }

            if($request->status == "create"){
                if(Department::where([
                                    ['code', '=', $request->department_code],
                                    ['location_id', '=', $request->location],
                                    ['is_deleted', '<>', '1']
                                    ])->exists()){
                                        return response()->json(['code'=>'1']);
                }else{
                    Department::updateOrCreate(['id' => $request->id],
                                                ['code' => $request->department_code,
                                                'name' => $request->department_name,
                                                'location_id' => $request->location,
                                                'is_deleted' => '0']);
                }
            }elseif($request->status == "edit"){
                if(Department::where([
                                    ['id', '=', Input::get('id')],
                                    ['code', '=', Input::get('department_code')],
                                    ['location_id', '=', Input::get('location')],
                                    ['is_deleted', '<>', '1']
                                    ])->exists()){
                                        Department::find(Input::get('id'))
                                                    ->update(['name' => $request->department_name,
                                                            'location_id' => $request->location,
                                                            'is_deleted' => '0']);
                }else{
                    if(Department::where([
                                        ['code', '=', Input::get('department_code')],
                                        ['location_id', '=', Input::get('location')],
                                        ['is_deleted', '<>', '1']
                                        ])->exists()){
                                            return response()->json(['code'=>'1']);
                    }else{
                        Department::find(Input::get('id'))
                                        ->update(['code' => $request->department_code,
                                                'name' => $request->department_name,
                                                'location_id' => $request->location,
                                                'is_deleted' => '0']);
                    }
                    
                }
            }

        return response()->json(['success'=>$request->status]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$location = Location::find($id);
        $data = DB::table('departments')
                ->join('locations', 'locations.id', '=', 'departments.location_id')
                ->select('departments.id as id', 'departments.code as deptcode', 'departments.name as deptname',
                        'locations.id as locid', 'departments.created_at as created_at', 'departments.updated_at as updated_at')
                ->where('departments.is_deleted', '<>', '1')
                ->where('departments.id', '=', $id)->orderBy('departments.id', 'ASC')->get();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department)
    {
        //
    }

    public function deleteDepartment($id)
    {
        Department::updateOrCreate(['id' => $id],
                                    ['is_deleted' => '1']);

        return response()->json(['successfully deleted']);
    }
}
