<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DataTables;
use DB;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('employees')
                ->join('departments', 'departments.id', '=', 'employees.department_id')
                ->leftjoin('locations', 'locations.id', '=', 'departments.location_id')
                ->select('employees.id as id', 'employees.employee_code as employee_code', 'employees.firstname as firstname',
                        'employees.lastname as lastname', 'employees.middlename as middlename', 'employees.is_deleted as is_deleted',
                        'locations.name as locname', 'departments.name as deptname', 'employees.created_at as created_at', 'employees.updated_at as updated_at')
                ->where('employees.is_deleted', '<>', '1')->orderBy('employees.id', 'ASC')->get();

        $areas = DB::table('departments')
                ->join('locations', 'locations.id', '=', 'departments.location_id')
                ->select('departments.id as id', 'departments.code as deptcode', 'departments.name as deptname',
                        'locations.name as locname', 'departments.created_at as created_at', 'departments.updated_at as updated_at')
                ->where('departments.is_deleted', '<>', '1')->orderBy('locname', 'ASC')->get();

        $locations = Location::where('is_deleted', '<>', '1')->orderBy('id', 'ASC')->get();

        if (request()->ajax()){
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editEmployee">Edit</a>';

                    $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteEmployee">Delete</a>';

                        return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('settings.employee.index', compact('areas', 'locations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'employee_code' => 'required|min:3',
            'firstname' => 'required|min:3',
            'middlename' => 'required|min:3',
            'lastname' => 'required|min:3',
            'area' => 'required',
        ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        if($request->status == "create"){
            if(Employee::where([
                                ['employee_code', '=', Input::get('employee_code')],
                                ['is_deleted', '<>', '1']])->exists()){
                                    return response()->json(['code'=>'1']);
            }else{
                Employee::updateOrCreate(['id' => $request->id],
                                                            ['employee_code' => $request->employee_code,
                                                                'firstname' => $request->firstname,
                                                                'middlename' => $request->middlename,
                                                                'lastname' => $request->lastname,
                                                                'department_id' => $request->area,
                                                                'is_deleted' => '0']);
            }
        }elseif($request->status == "edit"){
            if(Employee::where([
                                ['employee_code', '=', Input::get('employee_code')],
                                ['id', '=', Input::get('id')],
                                ['is_deleted', '<>', '1']
                                ])->exists()){
                                            Employee::find(Input::get('id'))
                                                            ->update(['firstname' => $request->firstname,
                                                                'middlename' => $request->middlename,
                                                                'lastname' => $request->lastname,
                                                                'department_id' => $request->area,
                                                                'is_deleted' => '0']);
            }else{
                if(Employee::where([
                                    ['employee_code', '=', Input::get('employee_code')],
                                    ['id', '<>', Input::get('id')],
                                    ['is_deleted', '<>', '1']
                                    ])->exists()){
                                        return response()->json(['code'=>'1']);
                }else{
                    Employee::find(Input::get('id'))
                                    ->update(['employee_code' => $request->employee_code,
                                        'firstname' => $request->firstname,
                                        'middlename' => $request->middlename,
                                        'lastname' => $request->lastname,
                                        'department_id' => $request->area,
                                        'is_deleted' => '0']);

                }
            }
        }

        return response()->json(['success'=>$request->status]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('employees')
                ->join('departments', 'departments.id', '=', 'employees.department_id')
                ->leftjoin('locations', 'locations.id', '=', 'departments.location_id')
                ->select('employees.id as id', 'departments.id as deptid', 'employees.employee_code as employee_code', 'employees.firstname as firstname',
                        'employees.lastname as lastname', 'employees.middlename as middlename', 'employees.is_deleted as is_deleted',
                        'locations.name as locname', 'departments.name as deptname', 'employees.created_at as created_at', 'employees.updated_at as updated_at')
                ->where('employees.is_deleted', '<>', '1')
                ->where('employees.id', '=', $id)
                ->orderBy('employees.id', 'ASC')->get();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        //
    }

    public function deleteEmployee($id)
    {
        Employee::updateOrCreate(['id' => $id],
                                    ['is_deleted' => '1']);

        return response()->json(['successfully deleted']);
    }
}
