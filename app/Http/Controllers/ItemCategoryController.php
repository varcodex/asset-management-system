<?php

namespace App\Http\Controllers;

use App\ItemCategory;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use DataTables;

class ItemCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ItemCategory::where('is_deleted', '<>', '1')->orderBy('id', 'ASC')->get();
        if (request()->ajax()){
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editItemCategory">Edit</a>';

                    $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteItemCategory">Delete</a>';

                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('settings.item_category.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'itemcategory_name' => 'required|min:3',
            'itemcategory_code' => 'required|min:3',
        ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        if($request->status == "create"){
            if(ItemCategory::where([
                                    ['code', '=', Input::get('itemcategory_code')],
                                    ['is_deleted', '<>', '1']
                                    ])->exists()){
                return response()->json(['code'=>'1']);
            }else{
                ItemCategory::updateOrCreate(['id' => $request->id],
                                                            ['code' => $request->itemcategory_code,
                                                                'name' => $request->itemcategory_name,
                                                                'is_deleted' => '0']);
            }
        }elseif($request->status == "edit"){
            if(ItemCategory::where([
                                ['code', '=', Input::get('itemcategory_code')],
                                ['id', '=', Input::get('id')],
                                ['is_deleted', '<>', '1']
                                ])->exists()){
                                    ItemCategory::find(Input::get('id'))
                                            ->update(['name' => $request->itemcategory_name,
                                                    'is_deleted' => '0']);
            }else{
                if(ItemCategory::where([
                                    ['code', '=', Input::get('itemcategory_code')],
                                    ['id', '<>', Input::get('id')],
                                    ['is_deleted', '<>', '1']
                                    ])->exists()){
                                        return response()->json(['code'=>'1']);
                    }else{
                        ItemCategory::find(Input::get('id'))
                                            ->update(['code' => $request->itemcategory_code,
                                                    'name' => $request->itemcategory_name,
                                                    'is_deleted' => '0']);
                    }
            }
        }

        return response()->json(['success'=>$request->status]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ItemCategory  $itemCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ItemCategory $itemCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ItemCategory  $itemCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $itemcategory = ItemCategory::find($id);
        return response()->json($itemcategory);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItemCategory  $itemCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ItemCategory $itemCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ItemCategory  $itemCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ItemCategory $itemCategory)
    {
        //
    }

    public function deleteItemCategory($id)
    {
        ItemCategory::updateOrCreate(['id' => $id],
                                    ['is_deleted' => '1']);

        return response()->json(['successfully deleted']);
    }
}
