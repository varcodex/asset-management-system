<?php

namespace App\Http\Controllers;

use App\ItemDetail;
use Illuminate\Http\Request;
use DataTables;
use DB;

class ItemDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('item_details')
                ->select('item_details.id as id', 'item_details.name as name', 'item_details.value as value',
                        'item_details.is_deleted as is_deleted','item_details.created_at as created_at',
                        'item_details.updated_at as updated_at')
                ->where('item_details.is_deleted', '<>', '1')->orderBy('item_details.id', 'ASC')->get();

        if (request()->ajax()){
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editItemList">Edit</a>';

                    $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteItemList">Delete</a>';

                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('settings.item_list.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ItemDetail  $itemDetail
     * @return \Illuminate\Http\Response
     */
    public function show(ItemDetail $itemDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ItemDetail  $itemDetail
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ItemDetail::select('id', 'name', 'value', 'datatype')
                            ->where('is_deleted', '<>', '1')
                            ->where('item_id', '=', $id)
                            ->orderBy('id', 'ASC')->get();
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItemDetail  $itemDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ItemDetail $itemDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ItemDetail  $itemDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(ItemDetail $itemDetail)
    {
        //
    }
}
