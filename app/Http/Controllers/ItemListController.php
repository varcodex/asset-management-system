<?php

namespace App\Http\Controllers;

use App\ItemList;
use App\ItemCondition;
use App\ItemDetail;
use App\ItemCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use DataTables;
use DB;
use Storage;

class ItemListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('item_lists')
                ->join('item_categories', 'item_categories.id', '=', 'item_lists.category_id')
                ->select('item_lists.id as id', 'item_lists.code as code', 'item_lists.name as name',
                        'item_categories.name as category', 'item_lists.created_at as created_at',
                        'item_lists.level', 'item_lists.updated_at as updated_at')
                ->where('item_lists.is_deleted', '<>', '1')->orderBy('item_lists.id', 'ASC')->get();

        if (request()->ajax()){
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="/itemlist/'.$row->id.'" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm viewItemList">View</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('settings.item_list.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = ItemCategory::where('is_deleted', '<>', '1')->orderBy('id', 'ASC')->get();
        return view('settings.item_list.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'category' => 'required',
            'item_name' => 'required|min:3',
            'date_purchased' => 'required|min:3',
            'warranty' => 'required',
            'file_name' => 'file|max:20000|mimes:pdf',
        ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        $specification = json_decode($request->specification, true);
        $condition = json_decode($request->condition, true);

        $status = false;
        while (!$status) 
        {
            if($request->code){
                $item_code = $request->code;
            }else{
                $item_code = str_random(10);
            }
            
            $result = ItemList::where('is_deleted', '<>', '1')->where('code', '=', $item_code)->orderBy('id', 'ASC')->get();

            if($result)
            {   
                
                $itemlist = ItemList::updateOrCreate(['id' => $request->id],
                                                    ['code' => $item_code,
                                                    'name' => $request->item_name,
                                                    'category_id' => $request->category,
                                                    'date_purchased' => $request->date_purchased,
                                                    'encoded_by' => Auth::id(),
                                                    'encoded_date' => now(),
                                                    'warranty' => $request->warranty,
                                                    'level' => '1',
                                                    'is_deleted' => '0'
                                                    ]);

                $status = true;

                if ($itemlist->wasRecentlyCreated) {
                    $this->storeFile($item_code, $request->file_name);
                }else{
                    if($request->file_name){
                        $image_path = public_path('_itemfiles').'\\'.$item_code.'.pdf';
                        unlink($image_path);
                        $this->storeFile($item_code, $request->file_name);
                    }
                }
                  
                
                
                for($i = 0; $i < count($specification); $i++){
                   ItemDetail::updateOrCreate(['id' => $specification[$i]['id']],
                                                ['item_id' => $itemlist->id,
                                                'name' => $specification[$i]['name'],
                                                'value' => $specification[$i]['value'],
                                                'datatype' => $specification[$i]['datatype'],
                                                'is_deleted' => '0'
                                                ]);
         
                }

                for($i = 0; $i < count($condition); $i++){
                    ItemCondition::updateOrCreate(['id' => $condition[$i]['id']],
                                                    ['item_id' => $itemlist->id,
                                                    'user_id' => Auth::id(),
                                                    'note' => $condition[$i]['note'],
                                                    'condition' => $condition[$i]['condition'],
                                                    'is_deleted' => '0'
                                                    ]);
        
                }

            }
        }

        return response()->json(['success'=>$request->status]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ItemList  $itemList
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categories = ItemCategory::where('is_deleted', '<>', '1')->orderBy('id', 'ASC')->get();
        $specs = ItemDetail::where('is_deleted', '<>', '1')->where('item_id', '=', $id)->orderBy('id', 'ASC')->get();
        $items = ItemList::where('is_deleted', '<>', '1')->where('id', '=', $id)->orderBy('id', 'ASC')->get();
        $conditions = ItemCondition::where('is_deleted', '<>', '1')->where('item_id', '=', $id)->orderBy('id', 'ASC')->get();
     
        return view('settings.item_list.edit', compact('categories', 'specs', 'conditions', 'items'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ItemList  $itemList
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItemList  $itemList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ItemList $itemList)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ItemList  $itemList
     * @return \Illuminate\Http\Response
     */
    public function destroy(ItemList $itemList)
    {
        //
    }

    private function storeFile($item_code, $uploadedFile)
    {
        $uploadedFile->move(public_path('_itemfiles'), $item_code.'.pdf');
    }

    public function itemReport()
    {
        $data = DB::table('item_lists')
                ->join('item_categories', 'item_categories.id', '=', 'item_lists.category_id')
                ->select('item_lists.id as id', 'item_lists.code as item_code', 'item_lists.name as item_name',
                        'item_categories.name as category', 'item_lists.date_purchased as date_purchased',
                        'item_lists.warranty as warranty', 'item_lists.created_at as created_at',
                        'item_lists.updated_at as updated_at')
                ->where('item_lists.is_deleted', '<>', '1')->orderBy('item_lists.id', 'ASC')->get();
        
             
        if (request()->ajax()){
            return Datatables::of($data)->make(true);
        }

        return view('report.itemreport');
    }

}
