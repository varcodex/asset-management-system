<?php

namespace App\Http\Controllers;

use App\Location;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use DataTables;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Location::where('is_deleted', '<>', '1')->orderBy('id', 'ASC')->get();
        if (request()->ajax()){
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editLocation">Edit</a>';

                    $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteLocation">Delete</a>';

                        return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('settings.location.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'location_name' => 'required|min:3',
            'location_code' => 'required|min:3',
        ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        if($request->status == "create"){
            if(Location::where([
                                ['code', '=', Input::get('location_code')],
                                ['is_deleted', '<>', '1']
                                ])->exists()){
                                    return response()->json(['code'=>'1']);
            }else{
                Location::updateOrCreate(['id' => $request->id],
                                                            ['code' => $request->location_code,
                                                                'name' => $request->location_name,
                                                                'is_deleted' => '0']);
            }
        }elseif($request->status == "edit"){
            if(Location::where([
                                ['code', '=', Input::get('location_code')],
                                ['id', '=', Input::get('id')],
                                ['is_deleted', '<>', '1']
                                ])->exists()){
                                    Location::find(Input::get('id'))
                                            ->update(['name' => $request->location_name,
                                                    'is_deleted' => '0']);
            }else{
                if(Location::where([
                                    ['code', '=', Input::get('location_code')],
                                    ['id', '<>', Input::get('id')],
                                    ['is_deleted', '<>', '1']
                                    ])->exists()){
                                        return response()->json(['code'=>'1']);
                }else{
                    Location::find(Input::get('id'))->update(
                        ['code' => $request->location_code,
                            'name' => $request->location_name,
                            'is_deleted' => '0']);
                }
                
            }
        }

        return response()->json(['success'=>$request->status]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show(Location $location)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $location = Location::find($id);
        return response()->json($location);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Location $location)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {

    }

    public function deleteLocation($id)
    {
        Location::updateOrCreate(['id' => $id],
                                    ['is_deleted' => '1']);

        return response()->json(['successfully deleted']);
    }
}
