<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Role;
use Session;

class UserController extends Controller
{

    public function index()
    {
        $users = User::sortable()->paginate(10);
        return view('users.index', compact('users'));
    }


    public function create()
    {
        $roles = Role::all();
        $user = new User();

        $task = 'Create User';
        return view('users.create', compact('user', 'roles', 'task'));
    }


    public function store(Request $request)
    {
        $data = request()->validate([
            'name' => 'required|min:3|unique:users',
            'email' => 'required|unique:users',
            'password' => 'required|min:8',
        ]);

        $user = User::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => Hash::make(request('password'))
        ]);

        $role_id = request('role');
        $user->roles()->attach($role_id);

        Session::flash('flash_message', 'User added successfully');
	    Session::flash('flash_type', 'alert-success');
        return back();
    }


    public function show(User $user)
    {
        return view('users.show', compact('user'));
    }


    public function edit(User $user)
    {
        $roles = Role::all();
        $task = 'User Details';
        return view('users.edit', compact('user', 'roles', 'task'));
    }


    public function update(User $user)
    {
        $data = request()->validate([
            'name' => 'required|min:3',
            'email' => 'required',
        ]);

        $user->name = request('name');
        $user->email = request('email');
        if ( ! request('password') == '')
        {
            $user->password = Hash::make(request('password'));
        }

        $user->save();
        $role_id = request('role');
        $user->roles()->detach();
        $user->roles()->attach($role_id);

        $roles = Role::all();
        $task = 'User Details';

        Session::flash('flash_message', 'User updated successfully');
	    Session::flash('flash_type', 'alert-success');
        return view('users.edit', compact('user', 'roles', 'task'));
    }


    public function destroy(User $user)
    {
        $user->delete();
        return back();
    }

    public function search(Request $request){
        $search = $request->get('search');
        $users = User::where('name', 'LIKE', "%{$search}%")
                    ->orWhere('email', 'LIKE', "%{$search}%")
                    ->sortable()->paginate(10);

        return view('users.index', compact('users'));
    }

}
