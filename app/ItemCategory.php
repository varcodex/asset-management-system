<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemCategory extends Model
{
    protected $guarded = [];
    public $table = "item_categories";
    protected $fillable = ['id', 'code', 'name', 'is_deleted', 'created_at', 'updated_at'];
}
