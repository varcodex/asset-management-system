<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemCondition extends Model
{
    protected $guarded = [];
    public $table = "item_conditions";
    protected $fillable = ['id', 'user_id', 'item_id', 'note', 'condition', 'is_deleted', 'created_at', 'updated_at'];
}
