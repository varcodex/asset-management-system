<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemDetail extends Model
{
    protected $guarded = [];
    public $table = "item_details";
    protected $fillable = ['id', 'item_id', 'datatype', 'name', 'value', 'is_deleted', 'created_at', 'updated_at'];
}
