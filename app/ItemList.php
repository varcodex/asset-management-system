<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemList extends Model
{
    protected $guarded = [];
    public $table = "item_lists";
    protected $fillable = ['id', 'code', 'name', 'category_id', 'date_purchased', 'warranty', 
                            'encoded_by', 'approved_by', 'noted_by', 'encoded_date', 'approved_date',
                            'noted_date', 'level', 'is_deleted', 'created_at', 'updated_at'];
}
