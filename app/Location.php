<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $guarded = [];
    public $table = "locations";
    protected $fillable = ['id', 'code', 'name', 'is_deleted', 'created_at', 'updated_at'];
}
