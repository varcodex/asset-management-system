<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\asset;
use Faker\Generator as Faker;

$factory->define(App\Asset::class, function (Faker $faker) {
    return [
        'control_no' => $faker->ean13,
        'assignee_name' => $faker->name,
        'asset_assigned' => $faker->word,
        'recieved_from' => $faker->name,
        'department_site' => $faker->jobTitle,
        'category' => $faker->word,
        'remarks' => $faker->sentence(),
        'status' => 'Completed',
        'created_at' => $faker->dateTime(),
        'updated_at' => $faker->dateTime(),
    ];
});
