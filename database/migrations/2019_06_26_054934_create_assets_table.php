<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('code')->nullable();
            $table->unsignedBigInteger('employee_id')->nullable();
            $table->unsignedBigInteger('item_id')->nullable();
            $table->string('encoded_by')->nullable();;
            $table->string('approved_by')->nullable();;
            $table->string('noted_by')->nullable();;
            $table->string('received_by')->nullable();;
            $table->string('released_by')->nullable();;
            $table->string('encoded_date')->nullable();;
            $table->string('approved_date')->nullable();;
            $table->string('noted_date')->nullable();;
            $table->string('received_date')->nullable();;
            $table->string('released_date')->nullable();;
            $table->string('transaction_type');
            $table->string('level');
            $table->string('is_deleted');
            $table->string('notes')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets');
    }
}
