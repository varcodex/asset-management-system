<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_lists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('name');
            $table->string('category_id');
            $table->string('date_purchased');
            $table->string('warranty');
            $table->string('encoded_by')->nullable();
            $table->string('approved_by')->nullable();
            $table->string('noted_by')->nullable();
            $table->string('encoded_date')->nullable();
            $table->string('approved_date')->nullable();
            $table->string('noted_date')->nullable();
            $table->string('level');
            $table->string('is_deleted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_lists');
    }
}
