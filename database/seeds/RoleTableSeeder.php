<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate();

        $admin = Role::create([
            'name' => 'Super User',
            'description' => 'A Super User'
        ]);
        
        $admin = Role::create([
            'name' => 'Head Staff',
            'description' => 'A head staff'
        ]);
        

        $staff = Role::create([
            'name' => 'Staff',
            'description' => 'A staff'
        ]);
        
    }
}
