<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        $role_super_user = Role::where('name', 'Super User')->first();
        $role_head_staff = Role::where('name', 'Head Staff')->first();
        $role_staff = Role::where('name', 'Staff')->first();


        $superuser = User::create([
            'name' => 'Administrator',
            'email' => 'administrator@xentromalls.com',
            'password' => Hash::make('L3tmein@amrc**')
        ]);
        $superuser->roles()->attach($role_super_user);

        $headstaff = User::create([
            'name' => 'Nenita Cruz',
            'email' => 'nsc@xentromalls.com',
            'password' => Hash::make('User@123?')
        ]);
        $headstaff->roles()->attach($role_head_staff);

        $staff = User::create([
            'name' => 'Eduardo Cruz',
            'email' => 'eduardo.cruz@xentromalls.com',
            'password' => Hash::make('User@123?')
        ]);
        $staff->roles()->attach($role_staff);

        $staff = User::create([
            'name' => 'Jay-Ann Requeza',
            'email' => 'jay-ann.requeza@xentromalls.com',
            'password' => Hash::make('User@123?')
        ]);
        $staff->roles()->attach($role_staff);

        $staff = User::create([
            'name' => 'Jonalyn Rivera',
            'email' => 'jonalyn.rivera@xentromalls.com',
            'password' => Hash::make('User@123?')
        ]);
        $staff->roles()->attach($role_staff);
    }
}
