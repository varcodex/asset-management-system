@extends('layout')
@section('content')


<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <p class="card-description">
                    Asset /<code>{{ $task }}</code>
                </p>
                        @include('messages.flashmessage')
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                </ul>

                            </div><br/>
                        @endif
                    <form class="forms-sample" action="/assets" method="POST" enctype="multipart/form-data">
                        @include('assets.form')

                        <button type="submit" id="submit" class="btn btn-primary mr-2">Add Asset</button>
                    </form>
            </div>
        </div>
    </div>
</div>

@endsection
