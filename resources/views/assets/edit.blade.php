@extends('layout')
@section('content')


<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <p class="card-description">
                    Asset /<code>{{ $task }}</code>
                </p>
                        @include('messages.flashmessage')
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                </ul>

                            </div><br/>
                        @endif

                    <form class="forms-sample" action="/assets/{{ $asset->id }}" method="POST" enctype="multipart/form-data">
                        @method('PATCH')
                        <div class="form-group">
                            <label for="Name">Control No.<code>*</code></label>
                            <label class="form-control bg-warning text-white" >{{ $asset->id }}</label>
                        </div>
                        @include('assets.form')

                        <button type="submit" id="submit" class="btn btn-primary mr-2">Update Asset</button>
                    </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        var value1 = $("#errorvalue1").text();
        var value2 = $("#errorvalue2").text();
        var value3 = $("#errorvalue3").text();
        var value4 = $("#errorvalue4").text();
        var value5 = $("#errorvalue5").text();
        if(value1){
            $("#error1").removeClass( "d-none" )
        }
        if(value2){
            $("#error2").removeClass( "d-none" )
        }
        if(value3){
            $("#error3").removeClass( "d-none" )
        }
        if(value4){
            $("#error4").removeClass( "d-none" )
        }
        if(value5){
            $("#error5").removeClass( "d-none" )
        }
        $("#submit").click(function(){
            $("#error1").show();
            $("#error2").show();
            $("#error3").show();
            $("#error4").show();
            $("#error5").show();
        });
    });
</script>

@endsection
