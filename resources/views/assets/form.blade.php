<div class="form-group">
    <label for="Asssigneen Name">Assignee Name<code>*</code></label>
    <input type="text" class="form-control" name="assignee_name" value="{{ old('assignee_name') ?? $asset->assignee_name }}" />
</div>

<div class="form-group">
    <label for="Asset Assigned">Asset Assigned<code>*</code></label>
    <input type="text" class="form-control" name="asset_assigned" value="{{ old('asset_assigned') ?? $asset->asset_assigned }}" />
</div>

<div class="form-group">
    <label for="Recieved From">Recieved From<code>*</code></label>
    <input type="text" class="form-control" name="recieved_from" value="{{ old('recieved_from') ?? $asset->recieved_from }}" />
</div>

<div class="form-group">
    <label for="Areas">Areas<code>*</code></label>
    <select class="form-control" name="asset_areas_id">
        <option disabled>Select a area</option>
        @foreach($areas as $area)
            <option value="{{ $area->id }}" {{ $area->id == $asset-> asset_areas_id ? 'selected' : '' }}>{{ $area->name }}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
<label for="Category">Category<code>*</code></label>
    <select class="form-control" name="asset_categories_id">
        <option disabled>Select a category</option>
        @foreach($categories as $category)
            <option value="{{ $category->id }}" {{ $category->id == $asset-> asset_categories_id ? 'selected' : '' }}>{{ $category->name }}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label for="Remarks">Remarks</label>
    <textarea class="form-control" name="remarks" id="exampleTextarea1" rows="4" >{{ old('remarks') ?? $asset->remarks }}</textarea>
</div>

<div class="form-group">
    <label for="File">Attach the supporting document(.pdf)<code>*</code></label>
    <input type="file" class="form-control" name="file_name"/>
</div>

@csrf
