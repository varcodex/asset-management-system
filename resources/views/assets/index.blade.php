@extends('layout')
@section('content')

<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <p class="card-description">
                    <code>Asset</code>
                </p>

                <div class="row">
                    <div class="col-6">
                        <div class="input-group">
                            <form action="assets/create">
                                <button type="submit" class="btn btn-primary btn-icon-text btn-rounded btn-fw btn-sm">
                                    <i class="mdi mdi-plus btn-icon-prepend"></i>Asset
                                </button>
                            </form>

                            <form action="/asset_export">
                                <button type="submit" class="btn btn-warning btn-icon-text btn-rounded btn-fw btn-sm ml-1">
                                    <i class="mdi mdi-file-export btn-icon-prepend"></i>Export CSV
                                </button>
                            </form>

                            <form action="/barcode-pdf">
                                <button type="submit" class="btn btn-success btn-icon-text btn-rounded btn-fw btn-sm ml-1">
                                    <i class="mdi mdi-file-export btn-icon-prepend"></i>Export Bacode
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="col-6">

                        <form action="/asset_search" method="GET">
                            <div class="input-group">
                                <input type="search" name="search" class="form-control" placeholder="Search . . .">
                                <span class="input-group-prepend">
                                    <button type="submit" class="btn btn-info">Search</button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="table-responsive pt-3">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>

                                <th>@sortablelink('id', 'Control No.')</th>
                                <th>@sortablelink('assignee_name', 'Name')</th>
                                <th>@sortablelink('asset_assigned', 'Asset')</th>
                                <!--
                                <th>@sortablelink('asset_categories_id', 'Category')</th>
                                -->
                                <th>@sortablelink('status', 'Status')</th>
                                <th>@sortablelink('created_at', 'Generated')</th>
                                <th>@sortablelink('updated_at', 'Last Update')</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($assets as $asset)
                                <tr>
                                    <td>
                                        {{ $asset->control_no }}
                                        {!!DNS1D::getBarcodeHTML($asset->control_no, 'S25')!!}
                                    </td>
                                    <td> {{ $asset->assignee_name }} </td>
                                    <td> {{ $asset->asset_assigned }} </td>
                                    <!--
                                    <td> {{ $asset->asset_categories->name }} </td>
                                    -->
                                    @if($asset->status == "Pending")
                                        <td><label class="badge badge-danger">{{ $asset->status}}</label></td>
                                    @elseif($asset->status == "Tag")
                                        <td><label class="badge badge-success">{{ $asset->status }}</label></td>
                                    @elseif($asset->status == "Untag")
                                        <td><label class="badge badge-warning">{{ $asset->status }}</label></td>
                                    @endif
                                    <td> {{ $asset->created_at }} </td>
                                    <td> {{ $asset->updated_at->diffForHumans() }} </td>
                                    <td>
                                        <form action="{{ route('assets.destroy',$asset->id) }}" method="POST">
                                            <a href="{{ route('assets.show',$asset->id) }}" class="mdi mdi mdi-eye btn-info"></a>
                                            <a href="{{ route('assets.edit',$asset->id) }}" class="mdi mdi-pencil btn-primary"></a>

                                            @csrf
                                            @method('DELETE')

                                            <button style="padding:0px" type="submit" class="mdi mdi-delete btn-icon btn-danger mt-1"
                                                    onclick="return confirm('Are you sure you want to permanently delete?')">
                                            </button>

                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>

                <div class="row">
                    <div class="col-12 d-flex justify-content-center pt-4">
                        {{ $assets->links() }}
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection
