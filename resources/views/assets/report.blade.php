@foreach($assets as $asset)
<table border="1">
        <thead>
            <tr>

                <th>{{ $asset->assignee_name }}</th>
            </tr>
        </thead>
        <tbody>
                <tr>
                    <td>
                        <div style="padding-left:10px;padding-bottom:10px;padding-top:10px;padding-right:10px;">
                            {{ $asset->control_no }}
                            {!!DNS1D::getBarcodeHTML($asset->control_no, 'S25')!!}
                            <label>Date Created: </label>{{ $asset->created_at->format('Y-m-d') }}
                        </div>
                    </td>
        </tbody>
</table>
@endforeach
<br>
