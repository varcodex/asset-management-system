@extends('layout')
@section('content')


<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <p class="card-description">
                    Asset /<code>Asset Details</code>
                </p>
                    @include('messages.flashmessage')
                    <form class="forms-sample" method="POST">
                        <div class="form-group">
                            <label for="Name">Control No.</label>
                            <label class="form-control">{{ $asset->control_no }}</label>
                        </div>

                        <div class="form-group">
                            <label for="Asssigneen Name">Assignee Name</label>
                            <label class="form-control">{{ $asset->assignee_name }}</label>
                        </div>


                        <div class="form-group">
                            <label for="Asset Assigned">Asset Assigned</label>
                            <label class="form-control">{{ $asset->asset_assigned }}</label>
                        </div>

                        <div class="form-group">
                            <label for="Recieved From">Recieved From</label>
                            <label class="form-control">{{ $asset->recieved_from }}</label>
                        </div>

                        <div class="form-group">
                            <label for="Areas">Areas</label>
                            <label class="form-control">{{ $asset->asset_areas->name }}</label>
                        </div>

                        <div class="form-group">
                            <label for="Category">Category</label>
                            <label class="form-control">{{ $asset->asset_categories->name }}</label>
                        </div>

                        <div class="form-group">
                            <label for="Remarks">Remarks</label>
                            <textarea class="form-control" name="remarks" id="exampleTextarea1" disabled rows="4">{{ $asset->remarks }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="Remarks">Supporting document</label>
                            <label class="form-control" name="file_name" id="exampleTextarea1"><a href="{{ asset('storage/uploads/'.$asset->file_name) }}" target="_blank">View this pdf file in your browser</a></label>
                        </div>

                        <div class="form-group">
                            <label for="Status">Status</label>
                            @if($asset->status == "Pending")
                                <label class="form-control badge-danger">{{ $asset->status }}</label>
                            @elseif($asset->status == "Tag")
                                <label class="form-control badge-success">{{ $asset->status }}</label>
                            @elseif($asset->status == "Untag")
                                <label class="form-control badge-warning">{{ $asset->status }}</label>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="Date Created">Date Created</label>
                            <label class="form-control">{{ $asset->created_at }}</label>
                        </div>

                        <div class="form-group">
                            <label for="Date Updated">Date Updated</label>
                            <label class="form-control">{{ $asset->updated_at }}</label>
                        </div>

                        <a class="btn btn-inverse-success mr-2 btn-md"
                            onclick="return confirm('Are you sure you want to do tag this asset?')"
                            href="/assets/{{ $asset->id }}/tag">Tag</a>

                        <a class="btn btn-inverse-warning mr-2 btn-md"
                            onclick="return confirm('Are you sure you want to do untag this asset?')"
                            href="/assets/{{ $asset->id }}/untag">Untag</a>



                    </form>
            </div>
        </div>
    </div>
</div>
@endsection
