@extends('layout')
@section('content')


<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body" id="myForm">
                <div class="d-flex">
                    <i class="mdi mdi-home text-muted hover-cursor"></i>
                    <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;<a href="/asset">Asset</a>&nbsp;/&nbsp;</p>
                    <p class="text-danger"><b>Create</b></p>
                </div>

                <div class="alert alert-danger" style="display:none"></div>

                <div class="d-flex">
                    <p class="text-danger"><b>Assignee Name<code>*</code></b></p>
                </div>

                <div class="table-responsive pt-3">
                    <table id="dtEmployee" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th></th>
                            <th class="th-sm">#</th>
                            <th class="th-sm">Employee Code</th>
                            <th class="th-sm">Firstname</th>
                            <th class="th-sm">Middlename</th>
                            <th class="th-sm">Lastname</th>
                            <th class="th-sm">Location</th>
                            <th class="th-sm">Department</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                          <tr>
                            <th></th>
                            <th class="th-sm">#</th>
                            <th class="th-sm">Employee Code</th>
                            <th class="th-sm">Firstname</th>
                            <th class="th-sm">Middlename</th>
                            <th class="th-sm">Lastname</th>
                            <th class="th-sm">Location</th>
                            <th class="th-sm">Department</th>
                          </tr>
                        </tfoot>
                      </table>
                </div>

                <br><hr><br>

                <div class="d-flex">
                    <p class="text-danger"><b>Item<code>*</code></b></p>
                </div>

                <div class="table-responsive pt-3">
                    <table id="dtItem" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th></th>
                            <th class="th-sm">#</th>
                            <th class="th-sm">Code</th>
                            <th class="th-sm">Name</th>
                            <th class="th-sm">Category</th>
                            <th width="280px">Action</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                          <tr>
                            <th></th>
                            <th class="th-sm">#</th>
                            <th class="th-sm">Code</th>
                            <th class="th-sm">Name</th>
                            <th class="th-sm">Category</th>
                            <th width="280px">Action</th>
                          </tr>
                        </tfoot>
                      </table>
                </div>

                <br><hr><br>

                <div class="d-flex">
                    <p class="text-danger"><b>Released by<code>*</code></b></p>
                </div>

                <div class="table-responsive pt-3">
                    <table id="dtRelease" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th></th>
                            <th class="th-sm">#</th>
                            <th class="th-sm">Employee Code</th>
                            <th class="th-sm">Firstname</th>
                            <th class="th-sm">Middlename</th>
                            <th class="th-sm">Lastname</th>
                            <th class="th-sm">Location</th>
                            <th class="th-sm">Department</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                          <tr>
                            <th></th>
                            <th class="th-sm">#</th>
                            <th class="th-sm">Employee Code</th>
                            <th class="th-sm">Firstname</th>
                            <th class="th-sm">Middlename</th>
                            <th class="th-sm">Lastname</th>
                            <th class="th-sm">Location</th>
                            <th class="th-sm">Department</th>
                          </tr>
                        </tfoot>
                      </table>
                </div>

                <br>

                <div class="form-group">
                    <label for="Date Released">Released date<code>*</code></label>
                    <input type="date" class="form-control" name="date_released" id="date_released" />
                </div>

                <br><hr><br>

                <div class="d-flex">
                    <p class="text-danger"><b>Receive by<code>*</code></b></p>
                </div>

                <div class="table-responsive pt-3">
                    <table id="dtReceive" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th></th>
                            <th class="th-sm">#</th>
                            <th class="th-sm">Employee Code</th>
                            <th class="th-sm">Firstname</th>
                            <th class="th-sm">Middlename</th>
                            <th class="th-sm">Lastname</th>
                            <th class="th-sm">Location</th>
                            <th class="th-sm">Department</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                          <tr>
                            <th></th>
                            <th class="th-sm">#</th>
                            <th class="th-sm">Employee Code</th>
                            <th class="th-sm">Firstname</th>
                            <th class="th-sm">Middlename</th>
                            <th class="th-sm">Lastname</th>
                            <th class="th-sm">Location</th>
                            <th class="th-sm">Department</th>
                          </tr>
                        </tfoot>
                      </table>
                </div>

                <br>

                <div class="form-group">
                    <label for="Date Received">Received date<code>*</code></label>
                    <input type="date" class="form-control" name="date_received" id="date_received" />
                </div>

                <div class="form-group">
                    <label for="File">Attach the supporting document(.pdf)<code>*</code></label>
                    <input type="file" class="form-control" name="file_name" id="file_name"/>
                </div>

                <br><hr><br>

                <div class="d-flex">
                    <p class="text-danger"><b>Asset Condition<code>*</code></b></p>
                </div>

                <button type="button" class="btn btn-inverse-primary btn-rounded btn-icon" id="createCondition" data-toggle="tooltip" data-placement="top" title="Create">
                    <i class="mdi mdi-playlist-plus"></i>
                </button>

                <div class="table-responsive pt-3">
                    <table id="dtCondition" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th class="th-sm"></th>
                            <th class="th-sm">Condition</th>
                            <th class="th-sm">Note</th>
                            <th class="th-sm">Action</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                          <tr>
                            <th class="th-sm"></th>
                            <th class="th-sm">Condition</th>
                            <th class="th-sm">Note</th>
                            <th class="th-sm">Action</th>
                          </tr>
                        </tfoot>
                      </table>
                </div>

                <br><br> 
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes</button>
                </div>
                <br>

                <div id="mItemDetails" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="classInfo" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                        <div class="modal-header">
                          
                            <h4 class="modal-title" id="classModalLabel">
                                Item Details
                            </h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                ×
                            </button>
                        </div>
                        <div class="modal-body">
                            <table id="dtItemDetails" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
                                <thead>
                                  <tr>
                                    <th class="th-sm">#</th>
                                    <th class="th-sm">Item Name</th>
                                    <th class="th-sm">Value</th>
                                    <th class="th-sm">Data Type</th>
                                  </tr>
                                </thead>
                                <tbody>
        
                                </tbody>
                                <tfoot>
                                  <tr>
                                    <th class="th-sm">#</th>
                                    <th class="th-sm">Item Name</th>
                                    <th class="th-sm">Value</th>
                                    <th class="th-sm">Data Type</th>
                                  </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-danger" data-dismiss="modal">
                            Close
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>

                <div class="modal fade" id="ConditionModel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            
                            <div class="modal-header">
                                <h4 class="modal-title" id="ConditionHeading"></h4>
                            </div>


                            <div class="card-body">
                                <form id="ConditionForm" name="ConditionForm" class="form-horizontal">
                                    <div class="specalert alert-danger" style="display:none"></div>
                                    <div class="form-group row">
                                        <label for="datatype" class="col-sm-3 text-right control-label col-form-label">Condition<code>*</code></label>
                                        <div class="col-sm-9">
                                            <select id="item_condition" name="item_condition" class="selectpicker" data-live-search="true" class="selectpicker show-tick">
                                                <option value="text">Text</option>
                                                <option value="number">Number</option>
                                                <option value="date">Date</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="itemdetail_name" class="col-sm-3 text-right control-label col-form-label">Note<code>*</code></label>
                                        <div class="col-sm-9">
                                            <textarea name="note" class="form-control" id="note" placeholder="Note Here"></textarea>
                                        </div>
                                    </div>

                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button class="btn btn-primary" id="addcondition">Add Item Condition</button>
                                    </div>
                                </form>
                            </div>
                            
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            var _jsonEmp = {!! json_encode($employees->toArray()) !!};
            var _jsonItem = {!! json_encode($items->toArray()) !!};
            var _iscondition = 0;
            var _empid_select = [];
            var _itemid_select = [];
            var _receiveid_select = [];
            var _releaseid_select = [];

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#dtEmployee tfoot th').not(":eq(0), :eq(1)").each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );

            $('#dtItem tfoot th').not(":eq(0), :eq(5)").each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );

            $('#dtRelease tfoot th').not(":eq(0), :eq(1)").each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );

            $('#dtItemDetails tfoot th').not(":eq(0)").each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );

            $('#dtReceive tfoot th').not(":eq(0), :eq(1)").each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );


            var tblEmp = $('#dtEmployee').DataTable({
                select: {
                    style: 'single',
                },
                order: [[ 1, 'desc' ]],
                orderCellsTop: true,
                fixedHeader: true
            });

            var tblItem = $('#dtItem').DataTable({
                select: {
                    style: 'single',
                },
                order: [[ 1, 'desc' ]],
                orderCellsTop: true,
                fixedHeader: true
            });

            var tblRelease = $('#dtRelease').DataTable({
                select: {
                    style: 'single',
                },
                order: [[ 1, 'desc' ]],
                orderCellsTop: true,
                fixedHeader: true
            });

            var tblReceive = $('#dtReceive').DataTable({
                select: {
                    style: 'single',
                },
                order: [[ 1, 'desc' ]],
                orderCellsTop: true,
                fixedHeader: true
            });

            var tblItemDetails = $('#dtItemDetails').DataTable({
                select: {
                    style: 'single',
                },
                order: [[ 0, 'desc' ]],
                orderCellsTop: true,
                fixedHeader: true
            });

            var tablecondition = $('#dtCondition').DataTable({
                select: {
                    style: 'single'
                },
                order: [[ 0, 'desc' ]],
                orderCellsTop: true
            });

            var i;
            for (i = 0; i < _jsonEmp.length; i++) {
                tblEmp.row.add( [
                    '<input type="radio" id="rEmp" name="rEmp" value="'+_jsonEmp[i]['id']+'">',
                    _jsonEmp[i]['id'],
                    _jsonEmp[i]['employee_code'],
                    _jsonEmp[i]['firstname'],
                    _jsonEmp[i]['lastname'],
                    _jsonEmp[i]['middlename'],
                    _jsonEmp[i]['locname'],
                    _jsonEmp[i]['deptname']
                ] ).draw(false);
            }

            var i;
            for (i = 0; i < _jsonItem.length; i++) {
                tblItem.row.add( [
                    '<input type="radio" id="rItem" name="rItem" value="'+_jsonItem[i]['id']+'">',
                    _jsonItem[i]['id'],
                    _jsonItem[i]['code'],
                    _jsonItem[i]['name'],
                    _jsonItem[i]['category'],
                    '<td><button type="button" name="view" id="view" class="btn btn-info btn-xs">View Details</button></td>'
                ] ).draw(false);
            }

            var i;
            for (i = 0; i < _jsonEmp.length; i++) {
                tblRelease.row.add( [
                    '<input type="radio" id="rReleaseby" name="rReleaseby" value="'+_jsonEmp[i]['id']+'">',
                    _jsonEmp[i]['id'],
                    _jsonEmp[i]['employee_code'],
                    _jsonEmp[i]['firstname'],
                    _jsonEmp[i]['lastname'],
                    _jsonEmp[i]['middlename'],
                    _jsonEmp[i]['locname'],
                    _jsonEmp[i]['deptname']
                ] ).draw(false);
            }

            var i;
            for (i = 0; i < _jsonEmp.length; i++) {
                tblReceive.row.add( [
                    '<input type="radio" id="rReceiveby" name="rReceiveby" value="'+_jsonEmp[i]['id']+'">',
                    _jsonEmp[i]['id'],
                    _jsonEmp[i]['employee_code'],
                    _jsonEmp[i]['firstname'],
                    _jsonEmp[i]['lastname'],
                    _jsonEmp[i]['middlename'],
                    _jsonEmp[i]['locname'],
                    _jsonEmp[i]['deptname']
                ] ).draw(false);
            }



            // Apply the search
            tblEmp.columns().eq( 0 ).each( function ( colIdx ) {
                if (colIdx == 0 || colIdx == 1) return; //Do not add event handlers for these columns

                $( 'input', tblEmp.column( colIdx ).footer() ).on( 'keyup change', function () {
                    tblEmp
                        .column( colIdx )
                        .search( this.value )
                        .draw();
                } );
            } );

            tblItem.columns().eq( 0 ).each( function ( colIdx ) {
                if (colIdx == 0 || colIdx == 5) return; //Do not add event handlers for these columns

                $( 'input', tblItem.column( colIdx ).footer() ).on( 'keyup change', function () {
                    tblItem
                        .column( colIdx )
                        .search( this.value )
                        .draw();
                } );
            } );

            tblRelease.columns().eq( 0 ).each( function ( colIdx ) {
                if (colIdx == 0 || colIdx == 1) return; //Do not add event handlers for these columns

                $( 'input', tblRelease.column( colIdx ).footer() ).on( 'keyup change', function () {
                    tblRelease
                        .column( colIdx )
                        .search( this.value )
                        .draw();
                } );
            } );

            tblReceive.columns().eq( 0 ).each( function ( colIdx ) {
                if (colIdx == 0 || colIdx == 1) return; //Do not add event handlers for these columns

                $( 'input', tblReceive.column( colIdx ).footer() ).on( 'keyup change', function () {
                    tblReceive
                        .column( colIdx )
                        .search( this.value )
                        .draw();
                } );
            } );

            tblItemDetails.columns().eq( 0 ).each( function ( colIdx ) {
                if (colIdx == 0) return; //Do not add event handlers for these columns

                $( 'input', tblItemDetails.column( colIdx ).footer() ).on( 'keyup change', function () {
                    tblItemDetails
                        .column( colIdx )
                        .search( this.value )
                        .draw();
                } );
            } );

            tblEmp.on('user-select', function (e, dt, type, cell, originalEvent) {
                if ($(cell.node()).parent().hasClass('selected')) {
                    e.preventDefault();
                }
            });
            tblItem.on('user-select', function (e, dt, type, cell, originalEvent) {
                if ($(cell.node()).parent().hasClass('selected')) {
                    e.preventDefault();
                }
            });
            tblRelease.on('user-select', function (e, dt, type, cell, originalEvent) {
                if ($(cell.node()).parent().hasClass('selected')) {
                    e.preventDefault();
                }
            });
            tblReceive.on('user-select', function (e, dt, type, cell, originalEvent) {
                if ($(cell.node()).parent().hasClass('selected')) {
                    e.preventDefault();
                }
            });
            tblItemDetails.on('user-select', function (e, dt, type, cell, originalEvent) {
                if ($(cell.node()).parent().hasClass('selected')) {
                    e.preventDefault();
                }
            });
            
            $(document).on('click', '#view', function(){
                tblItemDetails.clear();
                var item_id = [];
                tblItem.rows(this).select()
                $.each($("#dtItem tr.selected"),function(){ //get each tr which has selected class
                    item_id.push($(this).find('td').eq(1).text()); //find its first td and push the value
                    //dataArr.push($(this).find('td:first').text()); You can use this too
                });

                $.get("{{ route('itemdetail.index') }}" +'/' + item_id[0] +'/edit', function (data) {
                    var i;
                    var ctr = 1;
                    console.log(data);
                    for (i = 0; i < data.length; i++) {
                        tblItemDetails.row.add( [
                            ctr,
                            data[i].name,
                            data[i].value,
                            data[i].datatype
                        ] ).draw(false);
                        ctr++;
                    }
                    $('#mItemDetails').modal('show');
                })
            });

            $(document).on('click', '#rEmp', function(){
                _empid_select = [];
                $.each($("#dtEmployee tr.selected"),function(){ //get each tr which has selected class
                    _empid_select.push($(this).find('td').eq(1).text()); //find its first td and push the value
                    //dataArr.push($(this).find('td:first').text()); You can use this too
                });
            });

            $(document).on('click', '#rItem', function(){
                _itemid_select = [];
                $.each($("#dtItem tr.selected"),function(){ //get each tr which has selected class
                    _itemid_select.push($(this).find('td').eq(1).text()); //find its first td and push the value
                    //dataArr.push($(this).find('td:first').text()); You can use this too
                });
            });

            $(document).on('click', '#rReleaseby', function(){
                _releaseid_select = [];
                $.each($("#dtRelease tr.selected"),function(){ //get each tr which has selected class
                    _releaseid_select.push($(this).find('td').eq(1).text()); //find its first td and push the value
                    //dataArr.push($(this).find('td:first').text()); You can use this too
                });
            });

            $(document).on('click', '#rReceiveby', function(){
                _receiveid_select = [];
                $.each($("#dtReceived tr.selected"),function(){ //get each tr which has selected class
                    _receiveid_select.push($(this).find('td').eq(1).text()); //find its first td and push the value
                    //dataArr.push($(this).find('td:first').text()); You can use this too
                });
            });

            $('#createCondition').click(function () {
                $('#ConditionForm').trigger("reset");
                $('#ConditionHeading').html("Create Asset Condition");
                $('#ConditionModel').modal('show');
                $('.alert-danger').hide();
            });

            $('#addcondition').click(function () {
                 _note = $("#note").val();
                 _condition = $("#item_condition").val();
                _action = '<td><button type="button" name="removecondition" id="removecondition" class="btn btn-danger btn-xs">Remove</button></td>';
                _iscondition++;
                if(_note == null || _note == ""){
                    $('.specalert').html('Note is required');
                    $('.specalert').show();
                }else{
                    $('.specalert').html('');
                    $('.specalert').hide();
                    tablecondition.row.add( [
                        _iscondition,
                        _condition,
                        _note,
                        _action
                    ] ).draw(false);
                }

                return false;
            });

           
            $('#saveBtn').click(function () {
                
                var _id = '0';
                var _emp_id = '';
                var _item_id = '';
                var _received_by = '';
                var _release_by = '';

                var _emp_id = _empid_select;
                var _item_id = _itemid_select;
                var _received_by = _receiveid_select;
                var _release_by = _releaseid_select;
                var _date_received = document.getElementById("date_received").value;
                var _date_released = document.getElementById("date_released").value;
                var _file_name = $('#file_name').prop('files')[0];
                var _condition = [];
                var _con = tablecondition.rows().data();
                
                for(i=0; i<_con.length; i++){
                    _condition.push( {
                                    id : _con[i][0],
                                    condition : _con[i][1], 
                                    note : _con[i][2]
                                    } );
                }

                var form_data = new FormData()
                form_data.append('id', _id);
                form_data.append('employee', _emp_id);
                form_data.append('item', _item_id);
                form_data.append('received_by', _received_by);
                form_data.append('release_by', _release_by);
                form_data.append('date_received', _date_received);
                form_data.append('date_released', _date_released);
                form_data.append('file_name', _file_name);
                form_data.append('condition', JSON.stringify(_condition));

                if(! (document.getElementById("file_name").files.length == 0) && (tablecondition.data().count()) ){
                    $('.alert-danger').html('');
                    $.ajax({
                            data: form_data,
                            url: "{{ route('asset.store') }}",
                            type: "POST",
                            contentType: false, // The content type used when sending data to the server.
                            cache: false, // To unable request pages to be cached
                            processData: false,
                            success: function (data) {
                                if(data.errors){
                                    $('.alert-danger').html('');
                                    $('#saveBtn').html('Save changes');
                                    $.each(data.errors, function(key, value){
                                        $('.alert-danger').show();
                                        $('.alert-danger').append('<li>'+value+'</li>');
                                    });
                                }else{
                                    swal("Successful!", "New Asset created!", "success");
                                    document.getElementById("date_received").value = '';
                                    document.getElementById("date_released").value = '';
                                    $("#file_name").val('');
                                    tablecondition.clear().draw();
                                    $('.alert-danger').hide();
                                    console.log(data);
                                }
                            },
                            error: function (data) {
                                console.log('Error:', data);
                                jQuery('#saveBtn').html('Save Changes');
                            }
                    });
                }else{
                    if(document.getElementById("file_name").files.length == 0){
                            $('.alert-danger').show();
                            $('.alert-danger').append('<li>The supporting document is required.</li>');
                    }
                    if(!tablecondition.data().count()){
                            $('.alert-danger').show();
                            $('.alert-danger').append('<li>The item condition is required.</li>');
                    }
                }

            });


            $(document).on('click', '#removecondition', function(){
                if(confirm("Are you sure you want to remove this?")){
                    tablecondition.row('.selected').remove().draw(false);
                }
            });

        });
    </script>
@endpush
