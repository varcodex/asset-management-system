@extends('layout')
@section('content')

<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="d-flex">
                    <i class="mdi mdi-home text-muted hover-cursor"></i>
                    <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;</p>
                    <p class="text-primary mb-0 hover-cursor">Asset</p>
                </div>
                <br>
                

                <form action="{{ URL::to('asset/create') }}" method="GET">
                    <button type="submit" class="btn btn-inverse-primary btn-rounded btn-icon">
                        <i class="mdi mdi-database-plus"></i>
                    </button>
                </form>

                <div class="table-responsive pt-3">
                    <table id="dtBasicExample" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th class="th-sm">#</th>
                            <th class="th-sm">Asset Code</th>
                            <th class="th-sm">Employee Code</th>
                            <th width="th-sm">Item Code</th>
                            <th width="th-sm">Level</th>
                            <th width="th-sm">Transaction Type</th>
                            <th width="th-sm">Date Created</th>
                            <th width="th-sm">Last Update</th>
                            <th class="280px">Action</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                          <tr>
                            <th class="th-sm">#</th>
                            <th class="th-sm">Asset Code</th>
                            <th class="th-sm">Employee Code</th>
                            <th width="th-sm">Item Code</th>
                            <th width="th-sm">Level</th>
                            <th width="th-sm">Transaction Type</th>
                            <th width="th-sm">Date Created</th>
                            <th width="th-sm">Last Update</th>
                            <th class="280px">Action</th>
                          </tr>
                        </tfoot>
                      </table>
                </div>

                <div class="modal fade" id="ajaxModel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="alert alert-danger" style="display:none"></div>
                            <div class="modal-header">
                                <h4 class="modal-title" id="modelHeading"></h4>
                            </div>


                            <div class="card-body">
                                <form id="LocationForm" name="LocationForm" class="form-horizontal">
                                    <input type="hidden" name="id" id="id">
                                    <input type="hidden" name="status" id="status">
                                    <input type="hidden" name="temp" id="temp">

                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Code<code>*</code></label>
                                        <div class="col-sm-9">
                                            <input type="text" name="location_code" class="form-control" id="location_code" placeholder="Location Code Here">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Name<code>*</code></label>
                                        <div class="col-sm-9">
                                            <input type="text" name="location_name" class="form-control" id="location_name" placeholder="Location name Here">
                                        </div>
                                    </div>

                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection


@push('scripts')
    <script>
        $(document).ready(function () {


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#dtBasicExample tfoot th').not(":eq(8), :eq(0)").each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );

            var table = $('#dtBasicExample').DataTable({
                select: true,
                processing: true,
                serverSide: true,
                ajax: {
                    'url':"{{ route('asset.index') }}",
                    'type': 'GET',
                    'headers': {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },

                orderCellsTop: true,
                fixedHeader: true,
                columns: [
                        {data: 'id', name: 'id'},
                        {data: 'code', name: 'code'},
                        {data: 'employee_code', name: 'employee_code'},
                        {data: 'item_code', name: 'item_code'},
                        {data: 'level', name: 'level'},
                        {data: 'transaction_type', name: 'transaction_type'},
                        {data: 'created_at', name: 'created_at'},
                        {data: 'updated_at', name: 'updated_at'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ],
            order: [[0, 'desc']]
            });


        });
    </script>
@endpush
