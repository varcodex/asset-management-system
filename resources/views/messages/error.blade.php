<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Asset Management System</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/logo-title.png') }}"/>
        <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i" rel="stylesheet"> 
        <!-- Custom styles for this template -->
        <link rel="stylesheet" href="{{ asset('css/style1.css') }}">
    </head>
    <body class="content">
        <div class="container">
            <div class="row">
                <div class="main-content">
                    <div class="logo">
                        <h2 class="side-lines">AMRC Holdings Company Inc.</h2>
                    </div>
                    <div class="title">
                        <h1>501</h1>
                        <h5></h5>
                        <p>Sorry, access denied for a normal user.</p>
                    </div>
                    <div class="action-btn">
                        <a href="assets">Continue to Homepage</a>
                    </div>
                    <div class="copyright">
                        <div class="container">
                            &copy; Copyright © 2019 <strong>AMRC Holdings Company Inc.</strong> All rights reserved.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </body>
</html>