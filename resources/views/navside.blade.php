<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <!--
        <li class="nav-item">
            <a class="nav-link active" href="/">
                <i class="mdi mdi-file-document-box menu-icon"></i>
                <span class="menu-title">Asset</span>
            </a>
        </li>
        -->
        <li class="nav-item">
            <a class="nav-link active" href="/asset">
                <i class="mdi mdi-file-document-box menu-icon"></i>
                <span class="menu-title">Asset</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="mdi mdi-chart-areaspline menu-icon"></i>
              <span class="menu-title">Reports</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="{{ route('assetreport') }}">Asset</a></li>
                <li class="nav-item"> <a class="nav-link" href="{{ route('itemreport') }}">Item</a></li>

              </ul>
            </div>
        </li>

        <!--
            <li class="nav-item">
                <a class="nav-link" href="/users">
                    <i class="mdi mdi-account-multiple menu-icon"></i>
                    <span class="menu-title">User Account</span>
                </a>
            </li>
        -->

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="mdi mdi-settings menu-icon"></i>
              <span class="menu-title">Configuration</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="/location">Location</a></li>
                <li class="nav-item"> <a class="nav-link" href="/department">Department</a></li>
                <li class="nav-item"> <a class="nav-link" href="/itemlist">Item List</a></li>
                <li class="nav-item"> <a class="nav-link" href="/itemcategory">Item Category</a></li>
                <li class="nav-item"> <a class="nav-link" href="/employee">Employee</a></li>
                <li class="nav-item"> <a class="nav-link" href="/users">User Access</a></li>
                <li class="nav-item"> <a class="nav-link" href="">Profile</a></li>

              </ul>
            </div>
        </li>


    </ul>
</nav>
