<!DOCTYPE html >
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8" />
</head>
<style>
body {
  background: rgb(204,204,204); 
}
page {
  background: white;
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
  box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
}
page[size="A4"] {  
  width: 22cm;
  height: 36cm; 
}

@media print {
  body, page {
    margin: 0;
    box-shadow: 0;
  }
}

.content{
	padding: 50px;
}
.tbl{
	width: 100%;
	text-align: left;
}
</style>
<body style="margin: 4%;">
	<page size="A4">
		<div class='content'>
			<img src="{{ asset('1/img/1.png') }}"/>
			<br><br>
			<center><b><h3>ACKNOWLEDGEMENT RECEIPT AND ASSET ASSIGNMENT</h3></b></center>
			<br>
			<p align=”justify”>I, <b><u>{{ strtoupper($assets[0]->firstname) }} {{ strtoupper($assets[0]->middlename) }} {{ strtoupper($assets[0]->lastname) }}</b></u>
			HEREBY acknowledge that I have been issued the following asset/s by <b>AMRC HOLDINGS COMPANY INC.</b></p>
			<ul>
				@foreach($items as $item)
					<li><b>{{$item->name}} : </b>{{$item->value}}</li>
				@endforeach
			</ul>
			<br>
			<center><p>With my receipt of asset, I agree to abide by the following guidelines and procedures:</p></center>
			<ol>
				<li>I understand that <b>AMRC HOLDINGS COMPANY INC.</b> maintains the ownership of the asset at any time.</li>
				<li>I understand that the asset shall be prioritized for official business or for matters that involve company operations and I am the custodian of the asset.</li>
				<li>I understand that I have to use the asset properly and to keep the asset in a good working condition.</li>
				<li>I understand that <b>AMRC HOLDINGS COMPANY INC.</b> may authorize other personnel to use the asset at a specific time and for a specific reason. Other than such authorized users and on such specific reasons, the asset is for the sole use of the employee/s to which it has been assigned.</li>
				<li>I understand that<b>AMRC HOLDINGS COMPANY INC.</b> reserves the right to recover and/or transfer the assignment of the asset to another employee/s at any time.</li>
				<li>I understand that <b>AMRC HOLDINGS COMPANY INC.</b> that the asset shall not be leas  </li>
				<li>I understand that <b>AMRC HOLDINGS COMPANY INC.</b> allows the mobility and transport of said asset but must be properly handled and returned accordingly.</li>
				<li>I understand that I have to return the asset in case I proceed on long term leave or in case of separation from the company.</li>
				<li>I understand that any accessory not included in the assignment may be procured by the employee and used with the asset provided it does not interfere with the purpose of the company in assigning it to him or does it cause damage to the asset.</li>
				<li>I understand that proper care, storage and use of the asset is my sole responsibility.</li>
				<li>I understand that I shall recognize that the loss of or damage to the asset is not only a financial loss to the company but, likewise, a functionality loss and possibly loss of confidential or important data as well.</li>
				<li>I understand that in case of loss, I shall inform the Human Resources/IT Department/Admin Department within twenty four (24) hours after the loss has been discovered.</li>
				<li>I understand that in case loss or damage is due to negligence, the cost of the replacement or repair shall be on the account of the employee as such, I authorized the company to deduct the amount of the damaged or lost item assigned to me from my payroll.</li>
			</ol>
			<br><br>
			<table class="tbl">
				<tr>
				  <th>Received From:</th>
				  <th  style="padding-left: 30px;">Asset Assignee:</th>
				</tr>
				<tr>
				  <td><br><br><u>JONALYN D. RIVERA</u></td>
				  <td><br><br><u>{{ strtoupper($assets[0]->firstname) }} {{ strtoupper($assets[0]->middlename) }} {{ strtoupper($assets[0]->lastname) }}</u></td>
				</tr>
			</table>
			<br>
			<table class="tbl">
				<tr>
				  <td><br><br><u>CLAIRE MARGARETTE PASIONA<br><b>ADMIN OFFICER</b></u></td>
				</tr>
			</table>
			<br><br>
			<table class="tbl">
				<tr>
				  <th>Approved By:</th>
				  <th>Noted By:</th>
				</tr>
				<tr>
				  <td><br><br><u>NENITA SM. CRUZ<br><b>ADMIN MANAGER</b></u></td>
				  <td><br><br><u>CATHERINE DE VERA<br><b>HR AND ADMIN SENIOR MANAGER</b></u></td>
				</tr>
			</table>
		</div>
	</page>
</body>
</html>
