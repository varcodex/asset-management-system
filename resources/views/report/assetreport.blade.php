@extends('layout')
@section('content')

<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="d-flex">
                    <i class="mdi mdi-home text-muted hover-cursor"></i>
                    <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;</p>
                    <p class="text-primary mb-0 hover-cursor">Asset Report</p>
                </div>
                <br>

                <div class="table-responsive pt-3">
                    <table id="example" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th class="th-sm">#</th>
                            <th class="th-sm">Asset Code</th>
                            <th class="th-sm">Employee Code</th>
                            <th class="th-sm">Firstname</th>
                            <th class="th-sm">Middlename</th>
                            <th class="th-sm">Lastname</th>
                            <th width="th-sm">Location</th>
                            <th width="th-sm">Department</th>
                            <th width="th-sm">Item Code</th>
                            <th width="th-sm">Item Name</th>
                            <th width="th-sm">Transaction Type</th>
                            <th class="280px">Date Created</th>
                            <th class="280px">Date Update</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                          <tr>
                            <th class="th-sm">#</th>
                            <th class="th-sm">Asset Code</th>
                            <th class="th-sm">Employee Code</th>
                            <th class="th-sm">Firstname</th>
                            <th class="th-sm">Middlename</th>
                            <th class="th-sm">Lastname</th>
                            <th width="th-sm">Location</th>
                            <th width="th-sm">Department</th>
                            <th width="th-sm">Item Code</th>
                            <th width="th-sm">Item Name</th>
                            <th width="th-sm">Transaction Type</th>
                            <th class="280px">Date Created</th>
                            <th class="280px">Date Update</th>
                          </tr>
                        </tfoot>
                      </table>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection


@push('scripts')
    <script>
        $(document).ready(function () {


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#example tfoot th').not(":eq(0)").each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );

            var table = $('#example').DataTable({
                select: true,
                processing: true,
                serverSide: true,
                dom: 'Bfrtip',
                lengthChange: false,
                buttons: [ 'csv', 'excel', 'colvis' ],
                ajax: {
                    'url':"{{ route('assetreport') }}",
                    'type': 'GET',
                    'headers': {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },
                
                orderCellsTop: true,
                fixedHeader: true,
                columns: [
                        {data: 'id', name: 'id'},
                        {data: 'asset_code', name: 'asset_code'},
                        {data: 'employee_code', name: 'employee_code'},
                        {data: 'firstname', name: 'firstname'},
                        {data: 'middlename', name: 'middlename'},
                        {data: 'lastname', name: 'lastname'},
                        {data: 'location', name: 'location'},
                        {data: 'department', name: 'department'},
                        {data: 'item_code', name: 'item_code'},
                        {data: 'item_name', name: 'item_name'},
                        {data: 'transaction_type', name: 'transaction_type'},
                        {data: 'created_at', name: 'created_at'},
                        {data: 'updated_at', name: 'updated_at'},
                    ],
            order: [[0, 'desc']]
            });

            table.buttons().container()
            .appendTo( '#example_wrapper .col-md-6:eq(0)' );

            // Apply the search
            table.columns().eq( 0 ).each( function ( colIdx ) {
                if (colIdx == 0) return; //Do not add event handlers for these columns

                $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () {
                    table
                        .column( colIdx )
                        .search( this.value )
                        .draw();
                } );
            } );

        });
    </script>
@endpush