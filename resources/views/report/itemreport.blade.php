@extends('layout')
@section('content')

<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="d-flex">
                    <i class="mdi mdi-home text-muted hover-cursor"></i>
                    <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;</p>
                    <p class="text-primary mb-0 hover-cursor">Item Report</p>
                </div>
                <br>

                <div class="table-responsive pt-3">
                    <table id="example" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th class="th-sm">#</th>
                            <th class="th-sm">Item Code</th>
                            <th class="th-sm">Item Name</th>
                            <th class="th-sm">Date Purchased</th>
                            <th class="th-sm">Warranty</th>
                            <th class="th-sm">Category</th>
                            <th class="280px">Date Created</th>
                            <th class="280px">Date Updated</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                          <tr>
                            <th class="th-sm">#</th>
                            <th class="th-sm">Item Code</th>
                            <th class="th-sm">Item Name</th>
                            <th class="th-sm">Date Purchased</th>
                            <th class="th-sm">Warranty</th>
                            <th class="th-sm">Category</th>
                            <th class="280px">Date Created</th>
                            <th class="280px">Date Updated</th>
                          </tr>
                        </tfoot>
                      </table>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection


@push('scripts')
    <script>
        $(document).ready(function () {


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#example tfoot th').not(":eq(0)").each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );

            var table = $('#example').DataTable({
                select: true,
                processing: true,
                serverSide: true,
                dom: 'Bfrtip',
                lengthChange: false,
                buttons: [ 'csv', 'excel', 'colvis' ],
                ajax: {
                    'url':"{{ route('itemreport') }}",
                    'type': 'GET',
                    'headers': {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },
                
                orderCellsTop: true,
                fixedHeader: true,
                columns: [
                        {data: 'id', name: 'id'},
                        {data: 'item_code', name: 'item_code'},
                        {data: 'item_name', name: 'item_name'},
                        {data: 'date_purchased', name: 'date_purchased'},
                        {data: 'warranty', name: 'warranty'},
                        {data: 'category', name: 'category'},
                        {data: 'created_at', name: 'created_at'},
                        {data: 'updated_at', name: 'updated_at'},
                    ],
            order: [[0, 'desc']]
            });

            table.buttons().container()
            .appendTo( '#example_wrapper .col-md-6:eq(0)' );

            // Apply the search
            table.columns().eq( 0 ).each( function ( colIdx ) {
                if (colIdx == 0) return; //Do not add event handlers for these columns

                $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () {
                    table
                        .column( colIdx )
                        .search( this.value )
                        .draw();
                } );
            } );

        });
    </script>
@endpush