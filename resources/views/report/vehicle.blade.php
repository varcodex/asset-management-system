<!DOCTYPE html >
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8" />
</head>
<style>
body {
  background: rgb(204,204,204); 
}
page {
  background: white;
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
  box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
}
page[size="A4"] {  
  width: 21cm;
  height: 29.7cm; 
}

@media print {
  body, page {
    margin: 0;
    box-shadow: 0;
  }
}

.content{
	padding: 50px;
}
.tbl{
	width: 100%;
	text-align: left;
}
.signature {
    border: 0;
    border-bottom: 1px solid #000;
}
</style>
<body style="margin: 4%;">
	<page size="A4">
		<div class='content'>
			<img src="{{ asset('1/img/1.png') }}"/>
			<br>
			<div align="RIGHT">
			<H4>{{ NOW()->format('j-f-Y') }}</H4>
			</div>
			<br>
			<p align=”justify”><b>{{ strtoupper($assets[0]->firstname) }} {{ strtoupper($assets[0]->middlename) }} {{ strtoupper($assets[0]->lastname) }}</b><br>AMRC HOLDINGS COMPANY INC.</p>
			<br>
			
			<center><b><h3>VEHICLE PLAN / ASSIGNMENT</h3></b></center>
			<p><b>Dear {{ strtoupper($assets[0]->firstname) }}:</b></p>
			<p>Pleased confirm receipt of the VEHICLE with the following details and your concurrence to the attached company vehicle service program.</p>
			<ul>
				@foreach($items as $item)
					<li><b>{{$item->name}} : </b>{{$item->value}}</li>
				@endforeach
			</ul>
			<br>
			<p>All policies and procedures of the company in general and the company's specific Vehicle Plan shall apply (as detailed in the Company Vehicle Policy / 
				Car Plan Policy). Computation of Car plan sharing is attached on this document.
			</p>
			<p>Kindly sign on all pages of the attached vehicle plan and return the Admin.
			</p>
			
			<br><br>
			<table class="tbl">
				<tr>
				  <td><b>CLAIRE MARGARETTE PASIONA<br>Admin Officer</b></td>
				</tr>
			</table>
			<br>
			<table class="tbl">
				<tr>
				  <td><br><br>CATHERINE DE VERA<br><b>Admin / HR Senior Manager</b></td>
				</tr>
			</table>
			<br>
			<div align="RIGHT">
				<p>CONFORME: <b>{{ strtoupper($assets[0]->firstname) }} {{ strtoupper($assets[0]->middlename) }} {{ strtoupper($assets[0]->lastname) }}</b></p>
				<input type="text" class="signature" />
				<p ><small><i>Signature over Printed Name</i></small></p>
				<label><i>Date:</i></label><input type="text" class="signature" />
			</div>
			
			
		</div>
	</page>
</body>
</html>
