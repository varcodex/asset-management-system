@extends('layout')
@section('content')


<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body" id="myForm">
                <div class="d-flex">
                    <i class="mdi mdi-home text-muted hover-cursor"></i>
                    <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Settings&nbsp;/&nbsp;<a href="/itemlist">Item Lists</a>&nbsp;/&nbsp;</p>
                    <p class="text-danger"><b>Create</b></p>
                </div>
                <div class="alert alert-danger" style="display:none"></div>
                <div class="form-group">
                    <label for="Category">Category<code>*</code></label>
                    <select id="category" name="category" class="form-control selectpicker" data-live-search="true" class="selectpicker show-tick">
                        <option disabled>Select a category</option>
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
                <input type="hidden" name="item_id" id="item_id" value="">

                <div class="form-group">
                    <label for="Item Name">Name<code>*</code></label>
                    <input type="text" class="form-control" name="item_name" id="item_name" />
                </div>

                <div class="form-group">
                    <label for="Date Purchased">Date Purchased<code>*</code></label>
                    <input type="date" class="form-control" name="date_purchased" id="date_purchased" />
                </div>

                <div class="form-group">
                    <label for="Warranty">Warranty<code>*</code></label>
                    <input type="text" class="form-control" name="warranty" id="warranty" />
                </div>


                <div class="form-group">
                    <label for="File">Attach the supporting document(.pdf)<code>*</code></label>
                    <input type="file" class="form-control" name="file_name" id="file_name"/>
                </div>

                <br><hr><br>

                <div class="d-flex">
                    <p class="text-danger"><b>Item Specification<code>*</code></b></p>
                </div>
                <button type="button" class="btn btn-inverse-primary btn-rounded btn-icon" id="createDetails" data-toggle="tooltip" data-placement="top" title="Create">
                    <i class="mdi mdi-playlist-plus"></i>
                </button>

                <div class="table-responsive pt-3">
                    <table id="dtBasicExample" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th class="th-sm"></th>
                            <th class="th-sm">Name</th>
                            <th class="th-sm">Value</th>
                            <th class="th-sm">Data Type</th>
                            <th class="th-sm">Action</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                          <tr>
                            <th class="th-sm"></th>
                            <th class="th-sm">Name</th>
                            <th class="th-sm">Value</th>
                            <th class="th-sm">Data Type</th>
                            <th class="th-sm">Action</th>
                          </tr>
                        </tfoot>
                      </table>
                </div>

                <br><hr><br>

                <div class="d-flex">
                    <p class="text-danger"><b>Item Condition<code>*</code></b></p>
                </div>

                <button type="button" class="btn btn-inverse-primary btn-rounded btn-icon" id="createCondition" data-toggle="tooltip" data-placement="top" title="Create">
                    <i class="mdi mdi-playlist-plus"></i>
                </button>

                <div class="table-responsive pt-3">
                    <table id="dtCondition" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th class="th-sm"></th>
                            <th class="th-sm">Condition</th>
                            <th class="th-sm">Note</th>
                            <th class="th-sm">Action</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                          <tr>
                            <th class="th-sm"></th>
                            <th class="th-sm">Condition</th>
                            <th class="th-sm">Note</th>
                            <th class="th-sm">Action</th>
                          </tr>
                        </tfoot>
                      </table>
                </div>

                <br>

                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes</button>
                </div>

                <div class="modal fade" id="ajaxModel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            
                            <div class="modal-header">
                                <h4 class="modal-title" id="modelHeading"></h4>
                            </div>

                            <div class="card-body">
                                <div class="specalert alert-danger" style="display:none"></div>
                                <form id="ItemDetailForm" name="ItemDetailForm" class="form-horizontal">
                                    <div class="form-group row">
                                        <label for="datatype" class="col-sm-3 text-right control-label col-form-label">Data Type<code>*</code></label>
                                        <div class="col-sm-9">
                                            <select id="datatype" name="datatype" class="selectpicker" data-live-search="true" class="selectpicker show-tick">
                                                <option value="text">Text</option>
                                                <option value="number">Number</option>
                                                <option value="date">Date</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="itemdetail_name" class="col-sm-3 text-right control-label col-form-label">Name<code>*</code></label>
                                        <div class="col-sm-9">
                                            <input type="text" name="itemdetail_name" class="form-control" id="itemdetail_name" placeholder="Name Here">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="itemdetail_value" class="col-sm-3 text-right control-label col-form-label">Value<code>*</code></label>
                                        <div class="col-sm-9">
                                            <input type="text" name="itemdetail_value" class="form-control" id="itemdetail_value" placeholder="Value Here">
                                        </div>
                                    </div>

                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button class="btn btn-primary" id="add">Add Specification</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="modal fade" id="ConditionModel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            
                            <div class="modal-header">
                                <h4 class="modal-title" id="ConditionHeading"></h4>
                            </div>


                            <div class="card-body">
                                <form id="ConditionForm" name="ConditionForm" class="form-horizontal">
                                    <div class="specalert alert-danger" style="display:none"></div>
                                    <div class="form-group row">
                                        <label for="datatype" class="col-sm-3 text-right control-label col-form-label">Condition<code>*</code></label>
                                        <div class="col-sm-9">
                                            <select id="item_condition" name="item_condition" class="selectpicker" data-live-search="true" class="selectpicker show-tick">
                                                <option value="text">Text</option>
                                                <option value="number">Number</option>
                                                <option value="date">Date</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="itemdetail_name" class="col-sm-3 text-right control-label col-form-label">Note<code>*</code></label>
                                        <div class="col-sm-9">
                                            <textarea name="note" class="form-control" id="note" placeholder="Note Here"></textarea>
                                        </div>
                                    </div>

                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button class="btn btn-primary" id="addcondition">Add Item Condition</button>
                                    </div>
                                </form>
                            </div>
                            
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            var _ispec = 0;
            var _iscondition = 0;
            var _datatype = "";
            var _name = "";
            var _value = "";
            var select = document.getElementById('datatype');
            var _mod = 'Add';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#dtBasicExample').DataTable({
                select: {
                    style: 'single'
                },
                order: [[ 0, 'desc' ]],
                orderCellsTop: true
            });

            var tablecondition = $('#dtCondition').DataTable({
                select: {
                    style: 'single'
                },
                order: [[ 0, 'desc' ]],
                orderCellsTop: true
            });

            select.onchange = function() {
                $('#itemdetail_value').prop('type', select.value);
            }

            $('#createDetails').click(function () {
                $('#ItemDetailForm').trigger("reset");
                $('#modelHeading').html("Create Item Specification");
                $('#ajaxModel').modal('show');
                $('.alert-danger').hide();
            });

            $('#createCondition').click(function () {
                $('#ConditionForm').trigger("reset");
                $('#ConditionHeading').html("Create Item Condition");
                $('#ConditionModel').modal('show');
                $('.alert-danger').hide();
            });


            $('#add').click(function () {
                 _datatype = $("#datatype").val();
                 _name = $("#itemdetail_name").val();
                 _value = $("#itemdetail_value").val();
                _action = '<td><button type="button" name="remove" id="remove" class="btn btn-danger btn-xs">Remove</button></td>';
                _ispec++;

                if(_name == null || _name == ""){
                    $('.specalert').html('Name is required');
                    $('.specalert').show();
                }else if(_value == null || _value == ""){
                    $('.specalert').html('Value is required');
                    $('.specalert').show();
                }else{
                    $('.specalert').html('');
                    $('.specalert').hide();
                    table.row.add( [
                        _ispec,
                        _name,
                        _value,
                        _datatype,
                        _action
                    ] ).draw(false);
                }
                return false;
            });

            $('#addcondition').click(function () {
                 _note = $("#note").val();
                 _condition = $("#item_condition").val();
                _action = '<td><button type="button" name="removecondition" id="removecondition" class="btn btn-danger btn-xs">Remove</button></td>';
                _iscondition++;
                if(_note == null || _note == ""){
                    $('.specalert').html('Note is required');
                    $('.specalert').show();
                }else{
                    $('.specalert').html('');
                    $('.specalert').hide();
                    tablecondition.row.add( [
                        _iscondition,
                        _condition,
                        _note,
                        _action
                    ] ).draw(false);
                }

                return false;
            });

            $('#saveBtn').click(function () {
                var _id = '0';
                var _category = document.getElementById("category").value;
                var _item_name = document.getElementById("item_name").value;
                var _date_purchased = document.getElementById("date_purchased").value;
                var _warranty = document.getElementById("warranty").value;
                var _file_name = $('#file_name').prop('files')[0];
                var _specification = [];
                var _condition = [];
                var _spec = table.rows().data();
                var _con = tablecondition.rows().data();

                for(i=0; i<_spec.length; i++){
                    _specification.push( {
                                    id : _spec[i][0],
                                    name : _spec[i][1], 
                                    value : _spec[i][2],
                                    datatype : _spec[i][3]
                                    } );
                }

                for(i=0; i<_con.length; i++){
                    _condition.push( {
                                    id : _con[i][0],
                                    condition : _con[i][1], 
                                    note : _con[i][2]
                                    } );
                }

                var form_data = new FormData()
                form_data.append('id', _id);
                form_data.append('category', _category);
                form_data.append('item_name', _item_name);
                form_data.append('date_purchased', _date_purchased);
                form_data.append('warranty', _warranty);
                form_data.append('file_name', _file_name);
                form_data.append('specification', JSON.stringify(_specification));
                form_data.append('condition', JSON.stringify(_condition));

                if(! (document.getElementById("file_name").files.length == 0) && (table.data().count()) && (tablecondition.data().count())){
                    $('.alert-danger').html('');
                    $.ajax({
                            data: form_data,
                            url: "{{ route('itemlist.store') }}",
                            type: "POST",
                            contentType: false, // The content type used when sending data to the server.
                            cache: false, // To unable request pages to be cached
                            processData: false,
                            success: function (data) {
                                if(data.errors){
                                    $('.alert-danger').html('');
                                    $('#saveBtn').html('Save changes');
                                    $.each(data.errors, function(key, value){
                                        $('.alert-danger').show();
                                        $('.alert-danger').append('<li>'+value+'</li>');
                                    });
                                }else{
                                    swal("Successful!", "New item created!", "success");
                                    document.getElementById("item_name").value = '';
                                    document.getElementById("date_purchased").value = '';
                                    document.getElementById("warranty").value = '';
                                    $("#file_name").val('');
                                    table.clear().draw();
                                    tablecondition.clear().draw();
                                    $('.alert-danger').hide();
                                    console.log(data);
                                }
                            },
                            error: function (data) {
                                console.log('Error:', data);
                                jQuery('#saveBtn').html('Save Changes');
                            }
                    });
                }else{
                    if(document.getElementById("file_name").files.length == 0){
                            $('.alert-danger').show();
                            $('.alert-danger').append('<li>The supporting document is required.</li>');
                    }
                    if(!table.data().count()){
                            $('.alert-danger').show();
                            $('.alert-danger').append('<li>The item specification is required.</li>');
                    }
                    if(!tablecondition.data().count()){
                            $('.alert-danger').show();
                            $('.alert-danger').append('<li>The item condition is required.</li>');
                    }
                }
                

            });

            $(document).on('click', '#remove', function(){
                if(confirm("Are you sure you want to remove this?")){
                    table.row('.selected').remove().draw(false);
                }
            });

            $(document).on('click', '#removecondition', function(){
                if(confirm("Are you sure you want to remove this?")){
                    tablecondition.row('.selected').remove().draw(false);
                }
            });

        });
    </script>
@endpush
