@extends('layout')
@section('content')

<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="d-flex">
                    <i class="mdi mdi-home text-muted hover-cursor"></i>
                    <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Settings&nbsp;/&nbsp;</p>
                    <p class="text-primary mb-0 hover-cursor">Item List</p>
                </div>
                <br>

            <form action="{{ URL::to('itemlist/create') }}" method="GET">
                <button type="submit" class="btn btn-inverse-primary btn-rounded btn-icon">
                    <i class="mdi mdi-database-plus"></i>
                </button>
            </form>

                <div class="table-responsive pt-3">
                    <table id="dtBasicExample" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th class="th-sm">#</th>
                            <th class="th-sm">Code</th>
                            <th class="th-sm">Name</th>
                            <th class="th-sm">Category</th>
                            <th class="th-sm">Level</th>
                            <th class="th-sm">Date Created</th>
                            <th class="th-sm">Last Update</th>
                            <th width="280px">Action</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                          <tr>
                            <th class="th-sm">#</th>
                            <th class="th-sm">Code</th>
                            <th class="th-sm">Name</th>
                            <th class="th-sm">Category</th>
                            <th class="th-sm">Level</th>
                            <th class="th-sm">Date Created</th>
                            <th class="th-sm">Last Update</th>
                            <th width="280px">Action</th>
                          </tr>
                        </tfoot>
                      </table>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection


@push('scripts')
    <script>
        $(document).ready(function () {


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#dtBasicExample tfoot th').not(":eq(7), :eq(0)").each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );

            var table = $('#dtBasicExample').DataTable({
                select: true,
                processing: true,
                serverSide: true,
                ajax: {
                    'url':"{{ route('itemlist.index') }}",
                    'type': 'GET',
                    'headers': {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },
                orderCellsTop: true,
                fixedHeader: true,
                columns: [
                        {data: 'id', name: 'id'},
                        {data: 'code', name: 'code'},
                        {data: 'name', name: 'name'},
                        {data: 'category', name: 'category'},
                        {data: 'level', name: 'level'},
                        {data: 'created_at', name: 'created_at'},
                        {data: 'updated_at', name: 'updated_at'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ],
            order: [[0, 'desc']]
            });

            // Apply the search
            table.columns().eq( 0 ).each( function ( colIdx ) {
                if (colIdx == 7 || colIdx == 0) return; //Do not add event handlers for these columns

                $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () {
                    table
                        .column( colIdx )
                        .search( this.value )
                        .draw();
                } );
            } );

        });
    </script>
@endpush
