@extends('layout')
@section('content')

<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="d-flex">
                    <i class="mdi mdi-home text-muted hover-cursor"></i>
                    <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Settings&nbsp;/&nbsp;</p>
                    <p class="text-primary mb-0 hover-cursor">Location</p>
                </div>
                <br>
                <button type="button" class="btn btn-inverse-primary btn-rounded btn-icon" id="createLocation">
                    <i class="mdi mdi-database-plus"></i>
                </button>

                <div class="table-responsive pt-3">
                    <table id="dtBasicExample" class="table table-striped table-bordered display nowrap" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th class="th-sm">#</th>
                            <th class="th-sm">Code</th>
                            <th class="th-sm">Name</th>
                            <th class="th-sm">Date Created</th>
                            <th class="th-sm">Last Update</th>
                            <th width="280px">Action</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                          <tr>
                            <th class="th-sm">#</th>
                            <th class="th-sm">Code</th>
                            <th class="th-sm">Name</th>
                            <th class="th-sm">Date Created</th>
                            <th class="th-sm">Last Update</th>
                            <th width="280px">Action</th>
                          </tr>
                        </tfoot>
                      </table>
                </div>

                <div class="modal fade" id="ajaxModel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="alert alert-danger" style="display:none"></div>
                            <div class="modal-header">
                                <h4 class="modal-title" id="modelHeading"></h4>
                            </div>


                            <div class="card-body">
                                <form id="LocationForm" name="LocationForm" class="form-horizontal">
                                    <input type="hidden" name="id" id="id">
                                    <input type="hidden" name="status" id="status">
                                    <input type="hidden" name="temp" id="temp">

                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Code<code>*</code></label>
                                        <div class="col-sm-9">
                                            <input type="text" name="location_code" class="form-control" id="location_code" placeholder="Location Code Here">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Name<code>*</code></label>
                                        <div class="col-sm-9">
                                            <input type="text" name="location_name" class="form-control" id="location_name" placeholder="Location name Here">
                                        </div>
                                    </div>

                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection


@push('scripts')
    <script>
        $(document).ready(function () {


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#dtBasicExample').DataTable({
                select: true,
                processing: true,
                serverSide: true,
                ajax: {
                    'url':"{{ route('location.index') }}",
                    'type': 'GET',
                    'headers': {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },

                orderCellsTop: true,
                fixedHeader: true,
                columns: [
                        {data: 'id', name: 'id'},
                        {data: 'code', name: 'code'},
                        {data: 'name', name: 'name'},
                        {data: 'created_at', name: 'created_at'},
                        {data: 'updated_at', name: 'updated_at'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ],
            order: [[0, 'desc']]
            });

            //$('#dtBasicExampl').addClass('bs-select');

            $('#createLocation').click(function () {
                $('#saveBtn').val("create-location");
                $('#id').val('');
                $('#status').val('create');
                $('#LocationForm').trigger("reset");
                $('#modelHeading').html("Create New Location");
                $('#ajaxModel').modal('show');
                $('.alert-danger').hide();
            });

            $('body').on('click', '.editLocation', function () {
                var location_id = $(this).data('id');

                $.get("{{ route('location.index') }}" +'/' + location_id +'/edit', function (data) {
                    $('#modelHeading').html("Edit Location");
                    $('#status').val('edit');
                    $('#saveBtn').val("edit-location");
                    $('#ajaxModel').modal('show');
                    $('#saveBtn').html('Save changes');
                    $('#id').val(data.id);
                    $('#temp').val(data.name);
                    $('#location_code').val(data.code);
                    $('#location_name').val(data.name);
                })
            });

            $('#saveBtn').click(function (e) {
                e.preventDefault();
                $(this).html('Sending..');
                $('.alert-danger').hide();
                
                $.ajax({
                data: $('#LocationForm').serialize(),
                url: "{{ route('location.store') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    if(data.errors){
                        $('.alert-danger').html('');
                        $('#saveBtn').html('Save changes');
                        $.each(data.errors, function(key, value){
                            $('.alert-danger').show();
                            $('.alert-danger').append('<li>'+value+'</li>');
                  		});

                  	}else if(data.code == "1"){
                        $('.alert-danger').html('');
                        $('#saveBtn').html('Save changes');
                        $('.alert-danger').show();
                        $('.alert-danger').append('<li>Location Code Already Exists</li>');
                    }else{
                        $('#LocationForm').trigger("reset");
                        $('#ajaxModel').modal('hide');
                        $('.alert-danger').hide();
                        $('#saveBtn').html('Save changes');
                        table.draw();
                  	}

                },
                error: function (data) {
                    console.log('Error:', data);
                    jQuery('#saveBtn').html('Save Changes');
                }
                });
            });

            $('body').on('click', '.deleteLocation', function (){
                var id = $(this).data("id");


                swal({
                    title: "Do you want to delete this location?",
                    text: "You will not be able to recover this location!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn btn-success",
                    cancelButtonClass: 'btn btn-danger',
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel please!",
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: "GET",
                                url: "{{ route('location.index') }}" +'/' + id +'/remove',
                                success: function(data){
                                    swal("Deleted!", "Location has been deleted.", "success");
                                    table.draw();
                                },
                                error: function(data){
                                    console.log('Error', data);
                                }
                            });
                        } else {
                            swal("Cancelled", "Your imaginary file is safe :)", "error");
                        }
                });

           });

        });
    </script>
@endpush
