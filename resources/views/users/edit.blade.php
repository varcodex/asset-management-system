@extends('layout')
@section('content')


<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <p class="card-description">
                    User /<code>{{ $task }}</code>
                </p>
                    @include('messages.flashmessage')
                    <form class="forms-sample" action="/users/{{ $user->id }}" method="POST">
                        @method('PATCH')
                        @include('users.form')
                        <button type="submit" id="submit" class="btn btn-inverse-primary mr-2">Update User</button>
                    </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        var value1 = $("#errorvalue1").text();
        var value2 = $("#errorvalue2").text();
        var value3 = $("#errorvalue3").text();
        var value4 = $("#errorvalue4").text();
        var value5 = $("#errorvalue5").text();
        if(value1){
            $("#error1").removeClass( "d-none" )
        }
        if(value2){
            $("#error2").removeClass( "d-none" )
        }
        if(value3){
            $("#error3").removeClass( "d-none" )
        }
        if(value4){
            $("#error4").removeClass( "d-none" )
        }
        if(value5){
            $("#error5").removeClass( "d-none" )
        }
        $("#submit").click(function(){
            $("#error1").show();
            $("#error2").show();
            $("#error3").show();
            $("#error4").show();
            $("#error5").show();
        });
    });
</script>
@endsection
