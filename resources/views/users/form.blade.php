<div class="form-group">
    <label for="Name">Employee Name<code>*</code></label>
    <input type="text" class="form-control" name="name" value="{{ old('name') ?? $user->name }}" />
    <div id="error1" class="alert alert-danger alert-dismissible fade show d-none mt-1">
        <span id="errorvalue1">{{ $errors->first('name') }}</span>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
</div>


<div class="form-group">
    <label for="Username">Username<code>*</code></label>
    <input type="email" class="form-control" name="email" value="{{ old('email') ?? $user->email }}" />
    <div id="error2" class="alert alert-danger alert-dismissible fade show d-none mt-1">
        <span id="errorvalue2">{{ $errors->first('email') }}</span>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
</div>

<div class="form-group">
    <label for="Password">Password<code>*</code></label>
    <input type="password" class="form-control" name="password" value="{{ old('password') ??  '' }}" />
    <div id="error3" class="alert alert-danger alert-dismissible fade show d-none mt-1">
        <span id="errorvalue3">{{ $errors->first('password') }}</span>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
</div>

<div class="form-group">
<label for="role">Role<code>*</code></label>
    <select class="form-control" name="role">
        <option disabled>Select a Role</option>
        @foreach($roles as $role)
            <option value="{{ $role->id }}" {{ $user->hasRole($role->name) ? 'selected' : '' }}>{{ $role->name }}</option>
        @endforeach
    </select>

    <div id="error5" class="alert alert-danger alert-dismissible fade show d-none mt-1">
        <span id="errorvalue5">{{ $errors->first('role') }}</span>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
</div>
@csrf