@extends('layout')
@section('content')

<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <p class="card-description">
                    <code>User</code>
                </p>

                <div class="row">
                    <div class="col-6">
                        <form action="users/create">
                            <button type="submit" class="btn btn-primary btn-icon-text btn-rounded btn-fw btn-sm">
                                <i class="mdi mdi-plus btn-icon-prepend"></i>Asset
                            </button>
                        </form>
                    </div>
                    <div class="col-6">

                        <form action="/user_search" method="GET">
                            <div class="input-group">
                                <input type="search" name="search" class="form-control" placeholder="Search . . .">
                                <span class="input-group-prepend">
                                    <button type="submit" class="btn btn-info">Search</button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="table-responsive pt-3">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>@sortablelink('id', 'Control No.')</th>
                                <th>@sortablelink('name', 'Name')</th>
                                <th>@sortablelink('email', 'Username')</th>
                                <th>@sortablelink('created_at', 'Generated')</th>
                                <th>@sortablelink('updated_at', 'Last Update')</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td> {{ $user->id }} </td>
                                    <td> {{ $user->name }} </td>
                                    <td> {{ $user->	email }} </td>
                                    <td> {{ $user->	created_at }} </td>
                                    <td> {{ $user->	updated_at->diffForHumans() }} </td>
                                    <td>
                                        <form action="{{ route('users.destroy',$user->id) }}" method="POST">
                                            <a href="{{ route('users.show',$user->id) }}" class="mdi mdi mdi-eye btn-info"></a>
                                            <a href="{{ route('users.edit',$user->id) }}" class="mdi mdi-pencil btn-primary"></a>

                                            @csrf
                                            @method('DELETE')

                                            <button style="padding:0px" type="submit" class="mdi mdi-delete btn-danger"
                                                    onclick="return confirm('Are you sure you want to permanently delete?')">
                                            </button>

                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>

                <div class="row">
                    <div class="col-12 d-flex justify-content-center pt-4">
                        {{ $users->links() }}
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection
