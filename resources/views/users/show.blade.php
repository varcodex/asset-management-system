@extends('layout')
@section('content')


<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <p class="card-description">
                    User /<code>User Details</code>
                </p>
                    <form>
                        <div class="form-group">
                            <label for="Name">Control No.</label>
                            <label class="form-control">{{ $user->id }}</label>
                        </div>

                        <div class="form-group">
                            <label for="Name">Name</label>
                            <label class="form-control">{{ $user->name }}</label>
                        </div>
                        

                        <div class="form-group">
                            <label for="Username">Username</label>
                            <label class="form-control">{{ $user->email }}</label>
                        </div>

                        <div class="form-group">
                            <label for="Date Created">Date Created</label>
                            <label class="form-control">{{ $user->created_at }}</label>
                        </div>

                        <div class="form-group">
                            <label for="Date Updated">Date Updated</label>
                            <label class="form-control">{{ $user->updated_at }}</label>
                        </div>
                        
                        @csrf
                    </form>
            </div>
        </div>
    </div>
</div>

@endsection