<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',
            ['uses' => 'HomeController@index',
            'middleware' => ['auth', 'roles'],
            'roles' => ['Super User', 'Head Staff', 'Staff']
            ]);

//Asset
/*
Route::get('assets',
            ['uses' => 'AssetController@index',
            'middleware' => ['auth', 'roles'],
            'roles' => ['Super User', 'Head Staff', 'Staff']
            ]);

Route::get('assets/create',
            ['uses' => 'AssetController@create',
            'middleware' => ['auth', 'roles'],
            'roles' => ['Super User', 'Head Staff', 'Staff']
            ]);

Route::post('assets',
            ['uses' => 'AssetController@store',
            'middleware' => ['auth', 'roles'],
            'roles' => ['Super User', 'Head Staff', 'Staff']
            ]);

Route::get('assets/{asset}',
            ['uses' => 'AssetController@show',
            'middleware' => ['auth', 'roles'],
            'roles' => ['Super User', 'Head Staff', 'Staff']
            ])->name('assets.show');

Route::get('assets/{asset}/edit',
            ['uses' => 'AssetController@edit',
            'middleware' => ['auth', 'roles'],
            'roles' => ['Super User', 'Head Staff']
            ])->name('assets.edit');

Route::patch('assets/{asset}',
            ['uses' => 'AssetController@update',
            'middleware' => ['auth', 'roles'],
            'roles' => ['Super User', 'Head Staff']
            ]);

Route::delete('assets/{asset}',
            ['uses' => 'AssetController@destroy',
            'middleware' => ['auth', 'roles'],
            'roles' => ['Super User', 'Head Staff']
            ])->name('assets.destroy');

Route::get('assets/{asset}/tag',
            ['uses' => 'AssetController@tag',
            'middleware' => ['auth', 'roles'],
            'roles' => ['Super User', 'Head Staff']
            ])->name('tag');

Route::get('assets/{asset}/untag',
            ['uses' => 'AssetController@untag',
            'middleware' => ['auth', 'roles'],
            'roles' => ['Super User', 'Head Staff']
            ])->name('untag');

Route::get('/asset_export', 'AssetController@export');
Route::get('/barcode-pdf','AssetController@generatePDF');
*/

//User
Route::get('users',
            ['uses' => 'UserController@index',
            'middleware' => ['auth', 'roles'],
            'roles' => ['Super User']
            ]);

Route::get('users/create',
            ['uses' => 'UserController@create',
            'middleware' => ['auth', 'roles'],
            'roles' => ['Super User']
            ]);

Route::post('users',
            ['uses' => 'UserController@store',
            'middleware' => ['auth', 'roles'],
            'roles' => ['Super User']
            ]);

Route::get('users/{user}',
            ['uses' => 'UserController@show',
            'middleware' => ['auth', 'roles'],
            'roles' => ['Super User']
            ])->name('users.show');

Route::get('users/{user}/edit',
            ['uses' => 'UserController@edit',
            'middleware' => ['auth', 'roles'],
            'roles' => ['Super User']
            ])->name('users.edit');

Route::patch('users/{user}',
            ['uses' => 'UserController@update',
            'middleware' => ['auth', 'roles'],
            'roles' => ['Super User']
            ]);

Route::delete('users/{user}',
            ['uses' => 'UserController@destroy',
            'middleware' => ['auth', 'roles'],
            'roles' => ['Super User']
            ])->name('users.destroy');

Route::get('/user_search',
        ['uses' => 'UserController@search',
        'middleware' => ['auth', 'roles'],
        'roles' => ['Super User']
        ]);

//Location
/*
Route::get('location',
            ['uses' => 'LocationController@index',
            'middleware' => ['auth', 'roles'],
            'roles' => ['Super User']
            ])->name('location.index');

Route::get('location/{id}',
            ['uses' => 'LocationController@edit',
            'middleware' => ['auth', 'roles'],
            'roles' => ['Super User']
            ])->name('location.edit');

Route::post('locations/create',
            ['uses' => 'LocationController@store',
            'middleware' => ['auth', 'roles'],
            'roles' => ['Super User']
            ])->name('location.store');

//Department
Route::get('department',
            ['uses' => 'DepartmentController@index',
            'middleware' => ['auth', 'roles'],
            'roles' => ['Super User']
            ]);
 */

Route::resource('location','LocationController');
Route::resource('department','DepartmentController');

Route::resource('itemlist','ItemListController');
Route::get('/itemreport','ItemListController@itemReport')->name('itemreport');
Route::resource('itemcategory','ItemCategoryController');
Route::resource('itemdetail','ItemDetailController');

Route::resource('employee','EmployeeController');
Route::resource('asset','AssetController');
Route::get('/print/{code}','AssetController@print')->name('print');
Route::get('/assetreport','AssetController@assetReport')->name('assetreport');

Route::get('employee/{id}/remove',
            ['uses' => 'EmployeeController@deleteEmployee',
            'middleware' => ['auth', 'roles'],
            'roles' => ['Super User']
            ])->name('employee.remove');

Route::get('itemcategory/{id}/remove',
            ['uses' => 'ItemCategoryController@deleteItemCategory',
            'middleware' => ['auth', 'roles'],
            'roles' => ['Super User']
            ])->name('location.remove');

Route::get('location/{id}/remove',
            ['uses' => 'LocationController@deleteLocation',
            'middleware' => ['auth', 'roles'],
            'roles' => ['Super User']
            ])->name('location.remove');

Route::get('department/{id}/remove',
            ['uses' => 'DepartmentController@deleteDepartment',
            'middleware' => ['auth', 'roles'],
            'roles' => ['Super User']
            ])->name('location.remove');

//Authentication
Auth::routes();


Route::view('/ar', 'report.ar')->name('ar');

//Home
Route::get('/home', 'HomeController@index')->name('home');


//Error Message
Route::get('errormessage', function(){
    return view('messages.error');
})->middleware('auth');
